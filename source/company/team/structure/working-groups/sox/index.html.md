---
layout: markdown_page
title: "SOX PMO"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Attributes

| Property | Value |
|----------|-------|
| Date Created | March 11, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_SOX- PMO](https://gitlab.slack.com/messages/CGV47EU85) (only accessible from within the company) |
| Google Doc   | [SOX Working Group Agenda](https://docs.google.com/presentation/d/1PS_eHsoJAsjOq2cwYG7I3iSLz8sG9Upck-o6JSr8sDQ/edit?usp=sharing) (only accessible from within the company) 


## Business Goals

| Project Phase  | Estimated Completion Timeline  | Actual Completion Timeline | 
|---|---|---|
| Preliminary risk assessment | March - April 2019   | |
| Creation of SOPs (Detailed plan to be prepared)  | April 2019 onwards  | |
| SOX 404 documentation of process flows/control objectivees for business segments, including key control identification and test of design  | In sync with SOP creation  | |
| SOX 302 certification process  | Q3 2019  | |
| Development of aduit programs and evaluation of operating effectiveness of controls for FY 2020   | April/May 2020  | |
| Remediation & re-testing  | Prior to IPO  | |


## Exit Criteria 

Coming soon....



## Roles and Responsibilities

| Role                  | Responsibility                                                          |
|-----------------------|-------------------------------------------------------------------------|
| Facilitator           | Sheetal Jain |
| Functional Lead       | Cindy Nunez, Melody Gallagher, Melissa Farber                                   |
| Member                | Ramakrishnan Hariharan, Jamie Hurewitz, Kathy Wang                                              |
| Executive Stakeholder | Paul Machle |


