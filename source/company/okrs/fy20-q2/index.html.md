---
layout: markdown_page
title: "FY20-Q2 OKRs"
---

This fiscal quarter will run from May 1, 2019 to July 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. Grow website visitors. Introduce a certification program. Add a DevOps Transformation offering.

1. CRO: Create “what it looks like to partner with GitLab over the next 3 years” pitch for customers, present and test with at least 5 large enterprise customers.  (Map, Milestones, Outcomes)
1. CRO: Add a Devops Transformation Offering (I agree we want to test this but feel we are setting up a failed OKR unnecessarily by planing to create and deploy this in the next 90-ish days.)  We need to get the foundation in PS and CS set and this will be built with and on top of it.
1. CRO: Introduce a certification program.  This is already an objective for the training and enablement leader when they start.  Assigning it now, just before they are hired is not the right prioritization. (I think we are setting ourselves up with too broad of an objective at the wrong time on this one)
1. CPO
1. CMO
    1. Open a communication channel with Gitlab.com users. Implement a growth analytics and user communication tool in GitLab (self-managed & gitlab.com), Test an in-app message and hyperlink in Gitlab.com as an iteration, Redesign the gitlab.com/explore page.
    1. Re-focus on technical & how-to content.  Hire technical content re-mixer, XX number of blog and social technical content, SEO analysis of docs.
    1. Attract net-new developers and DevOps practitioners.  Deploy paid digital campaign focused on technical audience, enable retargeting of deeper technical content to docs visitors, launch GitLab "heroes" program, sign-up 1,500 users to 3 user conferences (500 each).
1. VP Product: Each PM conduct three customer discovery conversations with prospects per quarter
1. VP Eng: Stay on plan. Contribute X to revenue through growth, save X in infrastructure cost.
    1. Development:
        1. Growth: Increase Revenue. Raise retention metric from Xr to Yr, raise activation metrics from Xact to Yact, raise upsell metric from Xu to Yu, raise adoption metric from Xadp to Yadp.
1. VP Alliances: Close xxx deals through partner custom deals. Joint case study with customer highlighting the secure stage. Secure speaking slots at VMworld (US and EMEA) and ReInvent
1. VP Alliances: deliver shared partner strategy together with the new VP Channels

### CEO: Popular next generation product. Launch four growth groups. Deliver on maturity and new categories. Add consumption pricing for compute.

1. CRO: Easy to buy new, renewal and growth licenses. Redesign purchase experience from scratch, enable quick, easy and reliable license purchases, additions, and renewals, enable customers, partners and GitLab team to use the same system to quote and take orders and view/manage account.
1. CPO:
1. CMO
    1. Ensure appropriate transactional business/pricing. Analyze buying patterns of Starter & Bronze (user count, etc.), analyze web direct business, develop an iteration to low-end offerings based on data and solving potential conflicts with enterprise selling motions.
    1. Publish visual proof of benefits of single application. Benchmark toolchain clicks, handoffs and integrations, create methodology for comparing to GitLab.
1. VP Product: Hire Director of Product, Growth and 4 Growth PMs => Increase metrics…
Dissolve Growth working group
Execute and analyze 4 growth experiments for both self-managed and GitLab.com that improve key growth metrics
1. VP Product: 100% of direction items ship with usage metrics without an impact to velocity
1. VP Product: Deliver on our maturity plan
    1. CI/CD: Move one category each from Verify, and Release, and two from Package, from minimal to complete viability
1. VP Product: Add consumption pricing for macOS runners on GitLab.com => Metric should be about some quantity of consumption pricing sold
1. VP Product: Drive MAU for Release by 8% through establishing Progressive Delivery leadership in marketplace and making associated deliverables
1. VP Product: Drive MAU for Verify by 8% through synergies associated with Grand Unified Theory of CI/CD (high ROI primitives)
1. VP Eng: Increase productivity. Raise throughput from X to Y.
    1. Development: Increase productivity. Raise monthly throughput from ~1800 MRs  to ~2250.
        1. Dev: Increase productivity. Raise throughput from X to Y.
        1. CI/CD: Increase productivity. Raise throughput from X to Y.
        1. Ops: Increase productivity. Raise throughput from X to Y.
        1. Secure: Increase productivity. Raise throughput from X to Y.
        1. Defend: Increase productivity. Raise throughput from X to Y.
        1. Enablement: Increase productivity. Raise throughput from X to Y.
    1. Infrastructure: Make all user-visible services ready for mission critical workloads. Storage node migration to ZFS. Deliver Kubernetes. Deliver framework for cost management.
    1. Infrastructure: Control gitlab.com cost. Decrease cost per user from X to Y, Lower total target spend from X to Y.
    1. Quality: Make self-managed mission critical. All new bugs groomed in X timeframe and scheduled according to severity, all existing S1 S2 bugs scheduled successfully with priority and milestone, improve defect grooming visibility for engineering teams. Add 3 metrics into each group's dashboard view, average time to resolve bugs by severity, average time to schedule bugs, missed defect SLA. Provide missed defect SLA report for each group.
    1. Quality: Increase Engineering productivity. Improve defect detection feedback for engineering, cut down end-to-end test runtime in review apps by 50%, cut down review app deployment by 20%.
    1. Security: Secure the company, secure the product and customer data. 
    1. Security: Continue rollout of Zero Trust model.
        1. Application Security: HackerOne program spend on plan.
        1. Compliance: Develop and Publish Compliance Roadmap.
        1. Compliance: Complete Information Security Controls Gap Analysis.
    1. Support: Improve first contact Customer Experience. Build case deflection into support portal, increase docs linked to tickets to 75% (from 51%)
    1. Support: Establish consistent escalation path of customers issues. Define roles & responsibilities between tech support and customer success. Streamlined responsiveness to customer needs, consistent internal experience, developer happiness.
    1. UX: Improve usability. Raise System Usability Score (SUS) from ~74.6 to 76.2.
    1. UX: Great product. Pajamas design system, Address color contrast issues, user onboarding, convert GitLab.com prospects into users.
1. VP Alliances: deliver mature product that can license, bill and meter for our partner offering/marketplaces

### CEO: Great team. Become known as the company for all remote. Get all KPIs and OKRs from dashboards instead of slides. Handbook more accessible and SSoT. Become a world-class hiring and on-boarding company.

1. CFO: Improve accounting processes to support growth and public company reporting requirements.
    1. Controller: Audit completed by July 31, 2019
    1. Senior Technical Accounting Manager: 606 signed off by E&Y prior to end of April.  Other technical accounting issues for FY 18 resolved prior to end of May.
    1. Senior Accounting Manager: Achieve ten day close.
    1. Senior Internal Audit Manager: First phase of SoX compliance completed
        1. Standard Operating Procedures fully documented and included in handbook
        1. Test of Design of internal controls over financial reporting.
        1. Phase I - Test of operating effectiveness completed (For controls which passed the test of design)
1. CFO: Improve financial planning  processes to support growth and increased transparency.
    1. Dir. of Bus Ops: 100% of department dashboards and metrics pages completed with goals and definitions.
    1. Dir. of Bus Ops: BambooHR and Greenhouse ingestion fully automated.
    1. Dir. of Bus Ops:  Historical reporting from snapshots / log in place for SFDC.
    1. Dir. of Bus Ops: Data Quality Process (DQP) completed for ARR, Net and Gross Retention and Customer counts. Third party review completed for same metrics. https://docs.google.com/spreadsheets/d/1qcFD5UkOoG1P2pTfSEUvOkc9zk_vhqCDQR-oVkcB2zY/edit?usp=sharing
    1. FinOps: Fully integrated five year financial model that has gearing ratios documented in the handbook for non-headcount spending and headcount.
    1. FinOps: Metrics incorporated in financial model shown in periscope as KPIs with plan, forecast and actual data.
    1. FinOps: Financial results shown as plan vs actuals vs (forecast-tbd) at department level.
    1. Finance Business Partner: Show working model end to end for integrated financial modeling for support by May 15.
    1. Finance Business Partner:
        1. GitLab.com allocation model signed off by CFO and VP Engineering with handbook updated for methodology.
        1. Product Line P&L completed and issued on a monthly basis.
1. CFO: Create scalable infrastructure for achieving efficient growth
    1. VP of Legal, Contracts, IP and Compliance: 95% of team members covered by scalable employment solution
    1. Director of Business Operations: Procurement function created with leader in place.
    1. Director of Business Operations: First iteration of purchasing process changes merged into handbook.
    1. Director of Business Operations: GitLab Team Members are securely online Day 1
    1. Director of Business Operations: 100% Gitlabber Adoption of OKTA
    1. Director of Business Operations: OSX & Linux standards for purchasing are fully documented and the process is automated where available
        1. Less supported geos have purchasing guidelines documented
        1. Global Repair and Laptop Disposal policies documented
        1. Automate Laptop Deployment via Apple DEP
    1. Director of Business Operations: Evaluate and Install Client side security and provisioning on 50% of GitLab owned laptop fleet.
1. CPO:
    1. D&I / L&D: Enable an environment where all team members feel they belong and are engaged
        1. Defined key business metrics that should be improved by the training.
        1. Implement and iterate on new manager enablement program
        1. Develop and rollout professional communication training
        1. Onboard and enable our Diversity & Inclusion Partner
        1. Implement a diversity dashboard by department
    1. Compensation: Continue the evolution of our Compensation Model
        1. Ramp new Analyst successfully to understand our compensation model, working transparently in GitLab, and ensure proficiency in all related tools.
        1. Complete Phase 1 and 2 of our compensation iterations.
    1. Benefits: Commence a review of benefits in our team member locations
        1. Review Benefits for each payroll provider (entity and PEO) we have and ensure alignment to the global benefits guiding principles.
    1. People Operations: Continue to increase efficiencies across the People Ops team
        1. Transition to using Boards for projects to increase efficiency and transparency
        1. Fill open vacancies in the People Ops team - HRBP x2, People Ops Specialist, Web Content Manager
        1. Iterate further to streamline the Onboarding Issue to enhance our new hire experience
        1. Continue to support the conversion of contractors to our PEO - 10 countries during Q2
        1. Sign off on new HRIS system and have agreed implementation plan
    1. Recruiting: Deliver on the aggressive Hiring Plan in partnership with Leaders
    1. Employment Brand: Evolve GitLabs Employment Brand
        1. Collaborate with Marketing to create and publish a new GitLab recruiting video to promote working at GitLab and what it’s like to work at GItLab..
        1. Define and publish GitLab's employer value proposition.
        1. Identify 5 locations to focus our employment branding, recruitment marketing, and sourcing efforts and create campaign plans for these locations.
        1. Achieve "OpenCompany" status on Glassdoor; create and activate a process for gathering and responding to at least 50% of our Glassdoor reviews.
    1. Talent Operations: Enhance reporting capabilities
        1. Create scorecards for Recruiter, Sourcer, and Coordinator to measure effectiveness and balance workloads.
        1. Partner with Diversity Manager to Improve Diversity reporting capabilities and use it to influence recruiting processes and workflows to better eliminate bias.
        1. Build out leading indicators and evolve progress vs. plan dashboard
    1. Recruiting: Optimize current technology
        1. Evaluate ATS to renew/modify offerings with contract renewal in June.
        1. Consider Greenhouse Inclusion feature or other tools to remove bias.
        1. Find a solution to move from wet signature to electronic signatures for contracts in Germany
    1. Recruiting: Continue to identify process improvement opportunities and improve key metrics:
        1. Further leverage candidate survey data to inform process improvement opportunities.
        1. Initiate Hiring Manager Survey and action key insights.
        1. Continue to work towards stretch goal of 30 days for apply to accept metric
        1. Double the percentage of sourced hires from Q1 to Q2
        1. Standardize onboarding/training for new Recruiters, Coordinators, and Sourcers to provide efficient and effective ramp up with consistent ways of working.
        1. Hire executive recruiter to support more senior level roles / pull back on agency reliance
        1. Low location factor hiring goal consistent with department goals
1. CMO:
    1. Launch All-Remote web destination. Digest existing all-remote content, create an all-remote landing page for recruiting support, at least 2 all-remote related press articles in mainstream press (defined as non-tech press).
    1. 100% real-time funnel metrics. All funnel marketing metrics in periscope available to anyone at GitLab.
    1. Achieve Revenue Plan funnel targets. $13m in Net New IACV Pipeline created by beginning of Q3 FY20. 34,000 MQLs created, 11% MQL to SAO conversion rate.
1. VP Product:
    1. Get All KPIs built - with Business Ops
    1. Evangelize GitLab as the best-place to be a product manager.
        1. Key result - contribute 5 product management focused blog posts
        1. Key result - submit 10 CFPs
    1. Hire a great team - with recruiting
        1. Key result - Hire to 100% of plan
        1. Key result - Reduce time to offer to below company average
        1. Key result - Increase team diversity by 40%
    1. Promote cohesive team
        1. Key result - launch #pm_standup channel with multiple weekly engagements (customer, product, market)
        1. Key result - launch weekly Team Pulse survey
1. VP Eng: Get all KPIs built.
    1. Development: Achieve Level 3/4 Maturity for KPIs.  Key results Level three on Throughput, Average MRs/Dev/Month, Community First SLO response metric, BE Unit Test coverage, FE Unit Test coverage, Hiring actual vs. plan, Quality Metrics, CycleTime of MRs.
        1. Dev: Build KPI X.
        1. Ops: Build KPI X.
        1. Secure: Build KPI X.
        1. Growth: Build KPI X.
        1. Enablement: Build KPI X.
    1. Development: Hire to plan. X engaged candidates supplied by hiring manager, engagement with security-specific recruiting firm, X hires being.
        1. Dev: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. CI/CD: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. Ops: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. Secure: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. Defend: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. Growth: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. Enablement: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
    1. Infrastructure: Get all KPIs built.
    1. Quality: Get all KPIs built.
    1. Security: Get all KPIs built.
    1. Support: TBD objective. Get all KPIs built, transition staffing model blending productivity and revenue, update hiring plan for remainder of FY20 by region with Recruiting, update team page to reflect changes, iterate on Support roles definitions in Handbook.
    1. UX: Get all KPIs built.
1. VP Alliances: Launch marketing program for filling the acquisitions pipeline. Deliver 2 acquisitions to completion.
