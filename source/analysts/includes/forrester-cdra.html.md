---
layout: markdown_page
title: "Forrester Continuous Delivery and Release Automation Report"
---
## GitLab and The Forrester Wave™: Continuous Delivery and Release Automation, Q4 2018

This page represents how Forrester views our CDRA  capabilities in relation to the larger market and how we're working with that information to build a better product. It is also intended to provide Forrester with ongoing context for how our product is developing in relation to how they see the market.

A link to the report can be found [here](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q4+2018/-/E-RES141111). 

### Forrester's Key Takeaways on the CDRA Market at time of report publication:

**Electric Cloud, IBM, XebiaLabs, CA Technologies, and Microsoft lead the pack**
Forrester's research uncovered a market in which Electric Cloud, IBM, XebiaLabs, CA Technologies, and Microsoft are Leaders; CloudBees, Puppet, Micro Focus, and Flexagon are Strong Performers; Chef, Atlassian, GitLab, Inedo, and Octopus Deploy are Contenders; and CircleCI is a Challenger.

**I&O Pros Are Looking For An Expanding Mix Of CDRA Capabilities, From Source To Deploy**
The CDRA market is growing in breadth and functionality. CDRA tools model, deploy, and visualize application pipelines, orchestrating existing tools and assets. They also automate the delivery and release pipeline between development and production, supporting emerging use cases like containers, cloud-native architectures, and release readiness analytics.

**Robust Modeling, Platform Support, And Cognitive Capabilities Are Key Differentiators**
CDRA capabilities used to be the domain of separate tools — but no longer. Vendors are providing more robust integrated tooling and automation, including more sophisticated release modeling and choreography, as-code capabilities, application drift management, support for a broader spectrum of platforms, and increasing use of cognitive and machine learning.

### Lessons Learned and Future Improvements

While GitLab has maturity in areas such as Source Code Management (SCM) and [Continuous Integration (CI)](/analysts/forrester-ci/), ARO is a relatively new space for GitLab. Other tools in the report have been creating this functionality for years. Being on the Wave at this early a stage is an indicator that GitLab is now in this space and moving in the right direction. While other vendors may make it into many reports with many different products, GitLab is the only vendor who has be listed in multiple reports with the same product, which strongly re-enforces GitLab's message of using a single application for the entire DevOps lifecycle.

One area of functionality highlighted by the report that GitLab lacks is a release dashboard. Based on feedback from this report and customers, we plan to create [releases as first class entities](https://gitlab.com/gitlab-org/gitlab-ce/issues/32819) as well as a [release orchestration dashboard](https://gitlab.com/gitlab-org/gitlab-ee/issues/3277).

To learn more, view GitLab's [full vision for Release Orchestration](https://gitlab.com/groups/gitlab-org/-/epics/491 ).
