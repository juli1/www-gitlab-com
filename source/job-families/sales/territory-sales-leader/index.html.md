---
layout: job_family_page
title: "Territory Sales Leader"
---

This position is remotely based, ideally located in Territory.

## Responsibilities

* Act as a primary point of contact and the face of GitLab for our customers and prospects in your territory.
* Take ownership of the strategy and tactics to grow the incremental revenue in your territory by:
    * Selling direct and through the local channel.
    * Managing and supporting the channel partners in your territory to maximize the available opportunities for GitLab.
    * Providing direction to Demand Gen in order to conduct territory specific campaigns.
    * Assisting Field Marketing to participate in the appropriate activities for the territory.
    * Collaborating with the SDR’s in your territory for maximum account penetration.
    * Working with the Channel team to recruit any new partners needed to make your business more successful.
* Contribute to root cause analysis on wins/losses.
* Communicate lessons learned to the team, including account managers, the marketing team, and the technical team.
* Document the buying criteria.
* Document the buying process.
* Document next steps and owners.
* Ensure pipeline accuracy based on evidence and not hope.
* Contribute to documenting improvements in our sales handbook.
* Provide account leadership and direction in the pre- and post-sales process.
* Be the voice of the customer by contributing product ideas to our public issue tracker.

## Requirements

* A true desire to see customers benefit from the investment they make with you.
* 2+ years of experience with B2B sales.
* Interest in GitLab, and open source software.
* Ability to leverage established relationships and proven sales techniques for success.
* Effective communicator using the dominant language of the territory, strong interpersonal skills.
* Motivated, driven and results oriented.
* Excellent negotiation, presentation and closing skills.
* Preferred experience with Git, Software Development Tools, Application Lifecycle Management.
* You share our values, and work in accordance with those values.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with Regional Sales Director (in their same region)
* Candidates will then be invited to schedule a second interview with Regional Sales Director (in a different region)
* Candidates will be invited to schedule a third interview with our CRO
* Finally, candidates may be invited to an interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Compensation

You will typically get 50% as base and 50% based on commission. See our [market segmentation](/handbook/sales/#market-segmentation) for typical quotas in the U.S. Also see the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/).
