---
layout: job_family_page
title: "Field Marketing"
---

# Field Marketing Coordinator

A Field Marketing Coordinator is a regionally focused,  highly organized person who can do the hands-on work of managing logistics and details  for  events taking place in the region this role supports.  The Field Marketing Coordinator will work within the Field Marketing organization and will report into the regional Field Marketing Leader.

Field Marketing Coordinator Regions: 

North America 
EMEA

## Responsibilities 

* Assist Field Marketing Managers by securing suitable venues for regional events, per event type and budget.
* Manage all supporting logistics for field events, including printing, promotional items, supporting materials, shipping and receiving. 
* Assist in facilitating lead gathering and reporting.
* Research/execute third-party conferences and collect information for relevant programs across the regions to determine adjustments and improvements. 

## Requirements 

* 2+ years experience in marketing or event management with the desire to learn more about Field Marketing.
* Organized and efficient, able to develop and execute against a clear plan.
* Able to clearly communicate new ideas, products, and campaigns to both internal and external stakeholders.
* Team player with ability to work independently and autonomously.
* Flexibility to adjust to the dynamic nature of a startup.
* Occasional domestic travel to support events - less than 20%. 

## Hiring Process for North America 

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a screening call with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Manager, Field Marketing North America
- Then, candidates will be invited to schedule a second interview with one of our Regional Field Marketers
- Candidates will then be invited to schedule a third interview with our Regional Sales Development Managers
- Our CMO may choose to conduct an interview. 
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

## Hiring Process for EMEA 

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a screening call with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Field Marketinng Manager, EMEA
- Then, candidates will be invited to schedule a second interview with the Manager, Field Marketing North America
- Then, candidates will be invited to schedule a third interview with one of our SDR Managers 
- Candidates will then be invited to schedule a forth interview with one of our Regional Sales Directors
- Our CMO may choose to conduct an interview. 
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

# Field Marketing Manager 

The Field Marketing Manager position is responsible for all regional marketing supporting sales in their specific region. 

Field Marketing Manager Regions: 

Field Marketing Manager - US-West is responsible for supporting our US West Coast.

Field Marketing Manager - US-East is responsible for supporting our US East Coast. 

Field Marketing Manager - US-Public Sector is responsible for supporting our US Public Sector team. 

Field Marketing Manager - EMEA is responsible for supporting our Europe, Middle East, and Africa (EMEA) sales team.

Field Marketing Manager - APAC is responsible for supporting our Asia Pacific (APAC) sales team.

A successful Field Marketing Manager has strong understanding of sales-focused marketing as well as our audiences of enterprise IT leaders, IT ops practitioners, and developers. They enjoy taking charge of regional marketing programs, detailed planning, proactive communication, and flawlessly delivering memorable marketing experiences that support our sales objectives.

## Responsibilities

* Adapt digital and content marketing programs to the needs of a regional sales team.
* Create, expand, and accelerate sales opportunities through regional and account-based marketing execution, within marketing defined strategy.
* Be an advocate for the sales region you support and help the rest of the marketing department understand their priorities.
* Be an advocate for the marketing department and help the sales team you support understand the marketing department's priorities.
* Swag management for sponsored events, GitLab owned events, and in support of the sales department.
* Decision making and discretion regarding event selection and planning in support of regional sales objectives.
* Regional event strategy, decision making, and onsite management.
* Event logistics in support of the team. From helping to book space for meetings to making sure the booth is staffed, and making sure every aspect of our events are well organized.

## Requirements
- Past experience delivering, accelerating, and expanding sales pipeline through regional marketing.
- 5+ years of experience.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders.
- Ability to empathize with the needs and experiences of IT leaders, IT ops practicioners, and developers.
- Extremely detail-oriented and organized, and able to meet deadlines.
- You share our [values](/handbook/values/), and work in accordance with those values.
- A passion and strong understanding of the developer tools, IT operations tools, and/or IT security markets.
- Experience with supporting both direct sales and channel sales teams.

## Additional Requirements for EMEA
- Past experience running marketing to develop DACH, BENELUX, Nordics, and/or UK & Ireland.
- Strong understanding of marketing to the automotive, banking, and tech sectors.

## Additional Requirements for US Public Sector 
- Successful track record working with US Public Sector sales team,  distributors, and channel partners. 
- An understanding of state & local, Civilian, DoD, and Intelligence Community agencies and ability to translate this into a cohesive marketing plan for the Public Sector. 

# Senior Field Marketing Manager 
As Field Marketing Managers progress throughout their career at GitLab or we find the need to hire a Field Marketing Manager who has more years of relevant experience, there will be a need for a Senior Field Marketing Manager position with in the Field Marketing Organization. The Senior Field Marketing Manager will report into the Manager of Field Marketing for which region he/she reports into. 

## Responsibilities 
This role includes all of the responsibilities above, plus:
- Be a leader in working cross-functionally to drive the execution of demand generation and field marketing plans, aligning with other areas of marketing.
- Manage relevant agencies and 3rd parties in the execution of programs.
- Be willing to act as a Senior leader on the Field Marketing team, mentoring and guiding Field Marketing Coordinators and Managers.
- Be a leader in building and driving process within the Field Marketing organization. 

## Requirements 
This role includes all of the requirements above, plus:
- 10+ years of experience building and executing a regional strategy. 
- Proven ability to build and drive a budget within the territory.
- Experience in enterprise solution demand creation and field marketing.
- Strong orientation to managing program details.
- Travel up to 50% .

# Manager, North America Field Marketing
As GitLab grows, it will be important to have regional leadership to scale the field marketing activities. The Manager, North America Field Marketing, is a leadership role responsible for managing field marketers in North America, including both commercial and public sector activities. The Manager, North America Field Marketing will report to the Chief Marketing Officer.

## Responsibilities
- Manage a team of field marketers to develop and execute a regional demand generation strategy in line with company sales goals.
- Represent field marketing with field sales peers and management.
- Collaborate with senior marketing leaders, finance and sales leaders to create integrated plans and budgets.
- Be a player/coach, executing and managing marketing activities.
- Hire exceptional field marketers who align with GitLab values.

## Requirements
- The manager role includes all of the requirements above, plus:
- 10+ years of marketing experience
- Strong communication skills with senior marketing and sales leaders.
- Experience managing direct or cross-functional teams of 3-10 marketers.
- Experience managing a multi-million dollar marketing budget and working with senior finance and marketing leaders on annual budgeting processes.
- Demonstrated track record of driving results and staying within a budget.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with the CMO, Director of Field Marketing, regional sales leader, and a seller who he/she would support. 
- Depending on location, candidates may meet in person with any of the above. 
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email. The total compensation for this role listed in https://about.gitlab.com/job-families/marketing/field-marketing-manager/ is 100% base salary. 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
