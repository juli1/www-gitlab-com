---
layout: markdown_page
title: "Sales and Business Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}


As a Sales Development Representative (SDR) or Business Development Representative (BDR) you will be dealing with the front end of the sales process. Your focus will be on generating opportunities that the Account Executives (AE) and Strategic Account Leader (SAL) accept, ultimately leading to closed won business. On this team we work hard, but have fun too (I know, it's a cliche ...but here it's true!). We will work hard to guarantee your success if you do the same.

This handbook lays out the knowledge you will need to have in order to be successful in your role here at GitLab. Know that if you have the right mindset and come to work hungry with a positive attitude, you will see growth personally and in your career. Let's make it happen!

BDRs and SDRs (XDRs) use the [XDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128?&label_name[]=XDR) to track relevant GitLab issues involving the XDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following labels are used. 

XDR - Issues concerning the XDR team, general label to use intially
XDR in-flight - In-flight issues related to the XDR team
XDR top priority - Top priority issues related the XDR team

## GitLab Buyer
- [Video: Understand the GitLab Buyer: Personas and Pain Points](https://www.youtube.com/watch?v=-UITZi0mXeU&feature=youtu.be)
- [GitLab Roles and Personas](/handbook/marketing/product-marketing/roles-personas/)
- [Video: Chief Architect/Head of DevOps](https://www.youtube.com/watch?v=qyELotxsQzY)
- [Video: Head of IT](https://www.youtube.com/watch?v=LUh5eevH3F4)
- [Video: DevOps Director/Lead](https://www.youtube.com/watch?v=5_D4brnjwTg)

## GitLab Industry
- [Video: Understand the industry in which GitLab competes](https://www.youtube.com/watch?v=qQ0CL3J08lI)
- [DevOps Lifecycle](/stages-devops-lifecycle/)
- [Glossary of Terms](https://docs.gitlab.com/ee/university/glossary/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)

### DevOps
- [Video: What is DevOps?](https://www.youtube.com/watch?v=_I94-tJlovg)
- [Video: Why Auto DevOps Really Matters](https://www.youtube.com/watch?v=56chpCftwJQ&feature=youtu.be)
- [The State of DevOps 2017](https://www.bmc.com/content/dam/bmc/migration/pdf/Interop+ITX+State+of+DevOps_Final2-002-V1.pdf)
- [Video: Keynotes - DevOps Enterprise Summit 2017](https://www.youtube.com/playlist?list=PLvk9Yh_MWYuyq_rn0H-AP3ORoQ1QM0s1L)

### Competitive Landscape
- [Video: GitLab Competitive Landscape](https://www.youtube.com/watch?v=GDqGO5cv1Mk)
- [GitLab Compared](/devops-tools/)

## GitLab Solution
- [Video: Understand The GitLab Value Proposition](https://www.youtube.com/watch?v=6Dsdd1LSf40)
- [Video: Where GitLab Fits in Within Current Tool Stack](https://www.youtube.com/watch?v=YHznYB275Mg&feature=youtu.be)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Customer Use-Cases](/handbook/use-cases/)
- [Customer Stories](/blog/categories/customer-stories/)
- [GitLab Sales Demo](/handbook/marketing/product-marketing/demo/)
- [Video: Converting Free to Paid](https://www.youtube.com/watch?v=56eRRl6zFuA&feature=youtu.be)
- [Video: GitLab Sales Training - Security Products](https://www.youtube.com/watch?v=O1XFOlnywDI&feature=youtu.be)

## GitLab Messaging
Understand how to connect the GitLab buyer pain points to your calls, email messaging & social strategies

- [GitLab Elevator Pitch](/handbook/marketing/product-marketing/#elevator-pitch)
- [Video: Successful Messaging](https://www.youtube.com/watch?v=8LDlcvOgn9w)
- [Questions to Ask Yourself Before Sending an Email](/handbook/sales-training/#6-questions-sales-reps-should-but-dont-ask-themselves-before-sending-an-email-)
- [How to Write the Perfect Sales Email](https://www.saleshacker.com/how-to-write-the-perfect-sales-email/)
- [Voicemail Tips](https://blog.hubspot.com/sales/sales-voicemail-tips-that-guarantee-callbacks)
- [3 No Rule](https://docs.google.com/document/d/1aXeFKzwu5eGgGI6x_JrljIDL_ssTZrA7M7o0sAFNXVw/edit)

## How & When to Create an Opportunity

Process to follow how & when to create an opporunity is found in the [Business OPS](/handbook/business-ops/#how-to-create-an-opportunity) section of the handbook. 


## Training and Resources

You play a crucial role that helps bridge the gap between sales and marketing. As you gain knowledge, you will be able to aid our future customers ship better software, faster. There are numerous resources at your fingertips that we have created to help you in this process.

### Onboarding
You  will be assigned a GitLab Onboarding Issue. If you complete the BDR/SDR onboarding program within the defined time, you will receive the Super Leaf Coin. The BDR/SDR onboarding program provides you with the essential tools and and information in order to be successful as a BDR/SDR. The onboarding contains a prescriptive process and set of tasks for each day of your first 30 days of employment.

**Onboarding Assessments**

Throughout the BDR/SDR onboarding program there are assessments to test your knowledge on the learned topics from that day/week’s training. These quizzes are sent to your team lead automatically upon completion. Upon receipt, your team lead will review and discuss with you your score during your 1:1 time.

**How to assign the assessments to a new hire**
The team lead should notify the Marketing Operations Manager no later than one week before the new hire start date in order for the Marketo form (where the assesment templates live) to be updated to reflect the new hire name and corresponding team lead.

**Who are the results sent to and how do you know you passed?**
The assessment results are sent to the team lead overseeing the onboarding and will be reviewed in your weekly 1:1s throughout the duration of your onboarding. If not complete after 30 days you will recieve a written warning and you have one additonal week to complete them.


### Super Leaf Coin

When you've successfully passed your onboarding test you will receive this Super Leaf Challenge Coin because you now have eaten [The Super Leaf](https://www.mariowiki.com/Super_Leaf).

![Super Leaf Challenge Coin](img/super-leaf-challenge-coin.png)

More info about why this skill is so helpful to the tanuki is explained in the first item on [this page](https://en.wikipedia.org/wiki/Japanese_raccoon_dog#In_popular_culture).

### Process Resources

- [Business Operations Handbook](/handbook/business-ops/)
- [Marketing Handbook](/handbook/marketing/)
- [Sales Handbook](/handbook/sales/)
- [Customer Success Handbook](/handbook/customer-success/)
- [Resellers Handbook](/handbook/resellers/)
- [Support Handbook](/handbook/support/)

### Product Resources

- [GitLab Resources](/resources/)
- [GitLab Blog](/blog/)
- [GLU GitLab University](https://docs.gitlab.com/ee/university/)
- [GitLab Primer](/company/)
- [Sales Qualification Questions](/handbook/sales-qualification-questions/)
- [FAQ from Prospects](/handbook/sales-faq-from-prospects/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

### Tools

- [Tech Stack](/handbook/business-ops/#tech-stack)
- [Tools and Tips](/handbook/tools-and-tips/)
- [Getting Started with Slack](https://get.slack.help/hc/en-us/articles/218080037-Getting-started-for-new-members)
- [Outreach University](https://university.outreach.io/)
- How to Use Outreach - updated process coming soon
- [Outreach Killer Content & Using Sequences](https://university.outreach.io/live-training/80859)
- [Outreach Executing Tasks with Precision](https://university.outreach.io/live-training/80896)
- [Video: Outreach Advanced Training](https://drive.google.com/open?id=0BxIvWVJUiX7AZGNWdEZ5MHpCOHc)
- [Drift Agent Training Guide](https://help.drift.com/getting-started/drift-agent-training-guide)
- [Salesforce Trailhead Training](https://www.salesforce.com/services/training/overview/)
- [Video: LinkedIn Sales Navigator Training](https://www.youtube.com/watch?v=jxPw_pXqd2s&feature=youtu.be)
- [Drift Agent Training Guide](https://help.drift.com/getting-started/drift-agent-training-guide)
- [Drift Starter Guide](https://help.drift.com/getting-started/228660-the-ultimate-drift-starter-guide-for-sales-teams)

### Personal Development
- [Video: Time Management for Sales Development Success](https://www.youtube.com/watch?v=bA_xdjwAfZM&feature=youtu.be)
- [Video: How to Boost Your Business Acumen](https://www.youtube.com/watch?v=-h6XciVfraI)
- [Video: Business Acumen for Sales Professionals](https://www.youtube.com/watch?v=XRt9RIvxUTA)
- [Fanatical Prospecting](https://www.amazon.com/Fanatical-Prospecting-Jeb-Blount/dp/8126560053/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=&sr=)
- [Predictable Revenue](https://www.amazon.com/Predictable-Revenue-Business-Practices-Salesforce-com/dp/0984380213)
- [Predictable Prospecting: How to Radically Increase for B2B Sales Pipeline](https://www.amazon.com/Predictable-Prospecting-Radically-Increase-Pipeline/dp/1259835642/ref=sr_1_3?ie=UTF8&qid=1516560864&sr=8-3&keywords=prospecting)
- [The Challenger Sale](https://www.amazon.com/Challenger-Sale-Control-Customer-Conversation/dp/1591844355/ref=sr_1_1?ie=UTF8&qid=1516740978&sr=8-1&keywords=the+challenger+sale)
- [The Challenger Customer](https://www.amazon.com/Challenger-Customer-Selling-Influencer-Multiply/dp/1591848156/ref=sr_1_3?ie=UTF8&qid=1516740978&sr=8-3&keywords=the+challenger+sale)
- [Smart Calling: Eliminate the Fear, Failure, and Rejection from Cold Calling](https://www.amazon.com/Smart-Calling-Eliminate-Failure-Rejection-ebook/dp/B00C2BR56W)
- [Sales EQ: How Ultra High Preformers Leverage Sales-Specific Emotional Intelligence to Close the Complex Deal](https://www.amazon.com/Sales-Performers-Sales-Specific-Emotional-Intelligence/dp/B073HH4WPR/ref=sr_1_36?ie=UTF8&qid=1516560951&sr=8-36&keywords=prospecting)
- [High Profit Prospecting: Powerful Strategies to Find the Best Leads and Drive Breakthrough Sales Results](https://www.amazon.com/High-Profit-Prospecting-Powerful-Strategies-Breakthrough/dp/0814437761/ref=sr_1_4?ie=UTF8&qid=1516560899&sr=8-4&keywords=prospecting)
- [New Sales. Simplified.: The Essential Handbook for Prospecting and New Business Development](https://www.amazon.com/New-Sales-Simplified-Prospecting-Development-ebook/dp/B0094J7S9Y/ref=sr_1_2?ie=UTF8&qid=1516560864&sr=8-2&keywords=prospecting)
- [Hacker News](https://news.ycombinator.com/)
- [Martin Fowler](https://martinfowler.com/)
- [The New Stack](https://thenewstack.io/)
- [Top 13 DevOps Blogs You Should Be Reading](https://stackify.com/top-devops-blogs/)

## BDR & SDR Team Lead

### Weekly 1:1
- Current progress of onboarding program, if applicable
- Mental check-in (winning & success)
- Dashboard update & metrics overview
- Current prioritization & goals
- Coaching - email strategy, tools, campaigns, cadence, best practices (review recordings, if applicable)
- Review goals at the account level and personal level
- Follow up on action items from past meetings
- Upcoming events/campaigns that can be leveraged
- Personal goals & commitments
- Strategy for next week
- Recap and plan for next 1:1


## Asking questions

Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.
- [Who to Talk to for What](/handbook/product/#who-to-talk-to-for-what)

## Referring People to the Sales and Business Development Teams

Know someone who works at a company with at least 750 technical employees? Please introduce them to a Sales Development Representative Lead. When an SDR receives an introduction, they will respectfully ask the person that was introduced to them if they would be willing to point them to any people who are responsible for evaluating or selecting SCM, CI/CD, application monitoring, and/or software project management solutions. This approach helps ensure we're talking to the right people, and helps reduce the time it takes to identify the people within a company who would be best suited to hear about how GitLab can help their organization ship better software, faster.

You can use the following text to make an introduction over email:

```
Hi {contact.first.name},

My colleague {sdr.lead.first.name} has been trying to find people responsible for evaluating and selecting SCM and CI/CD solutions at {contact.organization}. Could you do me a favor and point them in the right direction? They would be asking to schedule a quick call to discuss the possibility of using GitLab at {contact.organization}. Thank you, I appreciate your help.

Best,

{first.name}
```

## How to BDR

{:toc}

[Video: Welcome to the Business Development Team](https://www.youtube.com/watch?v=dGbyVPSz9qY)

## BDR Standards
- Meet monthly quota of Sales Accepted Opportunities (SAOs)
- Strategize to develop the proper qualifying questions for all types of prospective customers.
- Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
- Have a sense of urgency - faster response time directly influences conversion rates.
- Generate Sales Accepted Opportunities (SAOs)
- Work lead records within Salesforce
- Maintain sense of ownership over data integrity & consistent activity logging
     - Includes adding complete **ACCOUNT** address details when an opportunity is created
     - Leveraging the data sources in order of enrichment priority (Datafox, DiscoverOrg, Clearbit)
- Do not touch any lead that has activity on it within the last 60 days by a different BDR.

### Flow
![BDR Process Flow](/images/handbook/marketing/bdrFlow.png){: .shadow}

It is imperative that you have an understanding of the following concepts to work effectively. Please be sure to review the following resources:

[Glossary](/handbook/business-ops/#glossary)

[Customer Lifecycle](/handbook/business-ops/#customer-lifecycle/)

[Lead Management](/handbook/business-ops/#lead-management)

[Video: Lead/Contact Management - Disposition Status](https://www.youtube.com/watch?v=lLHax2VPqdE&feature=youtu.be)

[Rules of Engagement](/handbook/business-ops/#rules-of-engagement)


## XDR Inbound Priority Process

### BDR Priority Process SFDC Work Views
#### P1 - MQLs-Request Contact
This will be your **top priority** view. Records with `Status` = `MQL` that have requested contact. Review and determine what outreach/status should be. `Status` and/or `Initial Source` is not restricting factor when a contact request is recieved. **All** must be `Accepted`, follow up completed then record updated with appropriate `Status` disposition  based on outcome.


#### P2 - MQLs excl SaaS trials   
Records with `Status` = `MQL` AND `MQL Date` = **LAST 60 DAYS** not including SaaS trials. Sort by `Last Interesting Moment Date` so they appear reverse chronological order (i.e. most recent at top). Sync records into Outreach enmasse, when possible, and insert into correct sequence.  
*If you sort by the last column, you can see if any of these leads has both an Self-hosted and SaaS trials      


#### P3 - Qualifying 
Records with `Status` = `Qualifying` which means at some point, you may have been engaged with them. Decide on next steps and use the "qualification notes" field to enter in the next step date and action needed. This will help you to prioritize from the lead view itself.


#### P4 - Qualified
Records with `Status` = `Qualified`. Please review, decide on next steps and disposition accordingly. (Typically should be minimal because if a records is `Qualified` the record should be converted to CONTACT and associated to correct ACCOUNT).


#### P5 - MQL-Trial - SaaS
Records with `Status` = `MQL` AND `Trial - SaaS` = `True` w/in the last 60 days. Sort records by `Evaluators` and place in high or low trial sequence.  


#### P6 - Accepted, showing action
Records with `Last Intersting Moment Date` OR `Last Activity` in the **past 60 days**.   
Review and disposition the `Status` correctly:  
- If you are not still actively working the record, **update the `Status` = `Nurture`**
- If records is unqualified, **update `Status` = `Unqualified`**   

Sort by `Title`, `No of Employees` and/or `Last Interesting Moment` to determine if record should remain in `Accepted` status. You can also sort by `Trial` columns to see if presently in or ever taken a trial/type.


#### P7 - Accepted, no action  
Records *without* a `Last Interesting Moment` and/or `Last Activity` within the **last 60 days**. 

Review and decide if you need to take an action or if the lead needs it's status updated.  
The purpose of this view is to see any bottlenecks, if behind on follow-up or forgot to disposition.    


#### P8 - Inquiry  
Records with `Status` = `Inquiry` sort by `Last Interesting Moment Date` (LIM). This is a "fishing pond".   
These records **have not yet** accumulated enough points (i.e. engaged with high value content) or completed a 'hand-raising' activity to be qualified as an `MQL`. 
Review the LIM description (if available) and `Company` to determine if worth bumping into an active outbound contact.   
Do not work to clear this view - this view will only be worked when P1 - P5 are completed.   

#### P9 - Stale MQL/Nurture w/new LIM
Records with `Status` = `MQL` OR `Nurture` AND `MQL Date` older than 30 days BUT `Last Interesting Moment Date` within 30 days or less.   
Do not work to clear this view - be aware of this view as records may appear in this view due to re-engagement campaigns.   

### SDR Record Handling Priority Process
Records - LEADS & CONTACTS - are generated by the Marketing team for XDR team who are responsible for qualifying the records **before** handoff to the Sales team. 


#### Qualification

Qualification is performed by the XDRs. **The best interest of the customer/prospect end user is to be paramount at all times**


#### Status Process
We must have an accurate reflection of the [record status](https://about.gitlab.com/handbook/business-ops/#lead--contact-statuses)(lead & contact) based on what the person engaged with, what is the status of followup and where they are in the lifecycle. 

The XDR is **100% responsible** for ensuring this status is accurate.    


## BDR Lead Research
Researching leads is an important first step. Not only can you leverage the information to build a rapport, but it will ensure data integrity.

Please be sure to update the following fields to ensure accuaracy:
- First and Last name (be sure this is capalitalized)
- Title
- Number of Employees

- **If a lead is confirmed to be a part of an existing customer group, please be sure to convert to the affiliated account prior to reassigning.**   
- **If there are duplicate records in the system, it is our job to clean them up by merging lead records and converting them into existing contacts, when applicable. When merging please retain the `Initial Source` that was created first as this is important to our attribution tracking.**
- **Total Employee Count is one of fields on the Lead that is used determine the Sales Segmentation for the Lead. If the Lead matches an Account in Salesforce via LeanData the Lead will adopt the Sales Segmentation of the Account. If the Lead does not match an Account and the Number of of Employees field on the lead is populated the Sales Segment of the Lead will be determined by value in this field. If all other fields return an `Unknown` segment or are blank then the Sales Segment on the Lead is determined by the value selected in the `Employee Bucket` field on the lead.**

**Personalized message research help and example:**

Three things in three minutes about the Organization, Industry, and Prospect

**Salesforce**

**Under the Account Record**
 * Is there an owned customer account?
 * Check for any alternative naming conventions
 * Are there any open opportunities?
 * Are there other leads from the organization that are being worked?
 * Who in the organization have we been in contact with?
 * Are there any duplicate leads or contacts?

**Under the Lead Record**
 * Review Salesforce Activity History
 * Has anyone been in contact with this lead previously?
 * Review the widgets built into Salesforce for further information on lead's actions.
   * Clearbit   
   * Lean Data
   *  Marketo Sales Insight
     * Interesting Moments
     * Web Activity
     * Emails (sent from Marketo)

**LinkedIn Sales Navigator**
* Confirm [Sales Segmentation](/handbook/business-ops/#segmentation) by determining `Total Employee` count for buiness.
* Person Summary
* Does anything stand out that is relevant to their needs as an organization?
* Have they published any articles that would be worth referencing?
* What is the lead’s role and how does that affect your messaging?
   * [Buyer personas](/handbook/marketing/marketing-sales-development/sdr/#gitlab-buyer)
* Previous work
   * Any Gitlab Customers that you can reference?
* Are they connected to anyone at GitLab for a possible introduction?

**Company Website**
- Mission Statement
- Press Releases/Newsroom
- (ctrl F) search for keywords


### Working with 3rd Parties

- Gather billing and end user details from the reseller:
- - Billing company name/address:
- - Billing company contact/email address:

- - End user company name/address:
- - End user contact/email address:
[Template in outreach](https://app1a.outreach.io/templates/814https://app1a.outreach.io/templates/814)
- Create new lead record with end user details
- - Ensure that all notes are copied over to new lead.
- Determine who the opportunity will be routed to based on the end user details

## BDR Prospecting
At GitLab, the top performing BDRs consistently follow up, experiment with a mix of calls, personalized emails, and non-traditional touch points in their prospecting cadence.

### Live Chat
We use a live chat tool - Drift - to facilitate two-way conversations with website visitors. As a BDR, you are expected to be active on Drift during your shift, with the exception of those times when you have to walk away or are taking a break. It is important to maintain a sense of urgency while watching the chat queue, sure to not miss any incoming inquiries.

See also: [Drift Starter Guide for Sales Teams](https://help.drift.com/getting-started/228660-the-ultimate-drift-starter-guide-for-sales-teams)

#### Drift Best Practices
You can see who else is online by viewing the [GitLab Team Profile](https://gitlabteam.drift.com/).

Notification settings:
- Go to `Browser Preferences` and select “Only conversations I’m in”.
- Desktop notifications will pop up every time you are added to the conversation.

When you are logged in and ready to chat, change your status to active by clicking “Turn off Away mode”

Pushing Conversation to Salesforce.com:
- Closing conversation when lead is qualified pushes conversation to Salesforce.com (either a new lead is created or conversation is attached to existing record).
- If you want to follow up with the site visitor, close the conversation, find record in SFDC and update necessary information.

Logging off:
- Go through all open conversations, mark the chats `Unqualified` if you are not following up with the site visitor, and close the conversation.
- “Set yourself to away” and close out window.

Conversation flow:
- Be helpful, but don’t allow yourself to become technical support. Refer to support as a paid feature, if necessary.
- Start conversations by obtaining name of the person and get an email if they haven’t supplied one. Determine company name as soon as possible.
- If the record is already assigned in Salesforce, alert the record owner and have them join the conversation, if possible.

Minor notes:
- If you leave the Drift page in your web browser, do not use the “back” button to return. The page will not be current. Follow a link or a bookmark. Be sure to hit the refresh button if you choose to use the “back” button.
- Drift conversations can be shared with others by copying the URL of the selected chat.


### Email
Tips
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

1. Subject line
2. Preview pain
3. Opening stanza
    - Make it about them, not about you
4. Benefit and value proposition
5. Call to action (CTA)
    - Offer available times for a meeting/call
    - Create urgency

### Working with Trials
During a trial, a prospect is automatically enrolled into a Marketo nurture stream for both self-hosted and SaaS products. The content of the nurture differs between a self-hosted Ultimate and a SaaS trial. Details of the self-hosted Ultimate Nuture can be found in this [Google presentation](https://docs.google.com/presentation/d/1KSAZFwz3nvSTIXOP8urGWW6dJWhtpawVKFcaoFLDPdg/edit#slide=id.g2ae1ad1112_0_22) (must be signed into GitLab account to view). 

Details of SaaS trial are being finalized; you can follow the issue [here](https://gitlab.com/gitlab-com/marketing/general/issues/2796).

You have access to and are expected to use (and customize) the [Ultimate master sequences](https://app1a.outreach.io/sequences?smart_view=202) in Outreach for measureable follow up for Self-managed and SaaS trial records.

### BDR Calling
[Video: Best Practices for Opening Calls](https://www.youtube.com/watch?v=jR1RGSBHo5Q&feature=youtu.be)

[Video: Best Practices for Asking Questions](https://www.youtube.com/watch?v=Lv4nozScm-M)

- Call about them, not about you
- Be confident and passionate
- Aim for every role but focus on technical decision makers
- Ask for time
- Focus on your endgame: Sales Accepted Opportunities (SAO)
- Make it easy to say yes
- Obtain a commitment - [Video: Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)

### BDR Prospecting Call Structure
- Introduction
    - Immediately introduce yourself and GitLab
    - Ask for time
- Recognize the inquiry
- Qualification
- Inform them/answer questions about GitLab
- Commitments

### Warm Prospecting Call Example
Purpose: To qualify leads faster and more accurately (Sales Acceptance Criteria) than in an email exchange.

Process: In your reply message to set up/initiate a call, ask a few of your normal BDR questions to prep for the call. To save a step in emails include your Calendly link so leads may schedule the call directly.

    “Hi (lead name), this is (BDR) from GitLab. How are you today?”

    “Great, is now still a good time to talk about (primary issue)?”

After you’ve established the conversation is good to move forward, ask questions and guide the conversation in a way that enables the lead to tell you what their issue/problem is while also providing answers to the sales acceptance criteria. Your primary role is to gather information, and decide more appropriately how to provide assistance and/or qualify the lead for a call with an AE.

    “Great (lead name), thank you for sharing that information. I have a better feel now for how to move forward with your request/issue. I’m going to follow-up with an email recapping what we discussed, send over that documentation I promised and get you in touch with (account executive)”

### Qualifying
[Video: Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)  
Your goal is to generate Sales Accepted Opportunities (SAOs) by gathering all pertinent information needed for an Account Executive to take action. Some examples of sales qualification questions can be found [here](/handbook/sales-qualification-questions/).

## BDR Compensation and Quota

### Variable Compensation
Full-time Inbound BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on individual SAO quota attainment.

Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. Inbound BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

- Team and individual quotas are based on GitLab's revenue targets
- Quotas will be made known by having each BDR sign a participant form that clearly lays out quarterly quotas that match the company's revenue plan
- Bonuses are based solely on sales accepted opportunities generated. Guidelines for a [sales accepted opportunity](/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao)
- Compensation will be paid monthly by the end of the following month.

There is an accelerator for BDRs who deliver over their SAO quota. SAOs are paid out based on progressive tiers of quota attainment:

* Base tier, 0-100%: Base Rate Payment (Monthly Variable @ Target/quota)
* Accelerator 1, 101%-200%: 1.1 times Base Rate Payment in excess of Quota
* Accelerator 2, > 200%: 1.5 times Base Rate Payment in excess of Accelerator 1
* There is no floor (except for 200% for ramping reps)

### Additional Measurements
BDRs will also be measured on the following:

* Results
  * Pipeline value of BDR generated opportunities
  * IACV won from opportunities BDR generated
* Activity
  * Number of opportunities created
  * Number of emails sent
  * Number of calls made

Note, while important, the above measurements do not impact your quota attainment. Your main focus will be achieving your SAO quota.

Handover to SDRs

For an inbound lead that which is the correct contact with interest but requires further touches and qualifications the following process should occur:

*BDR confirms all details, including the telephone number, company and email address
*BDR passes the lead to SDR working in that territory or account as a Pre-Qualified Lead
*SDR accepts the lead and converts the status to Pre-Qualified Lead Accepted and begins to work on it to progress it to the opportunity

**BDR compensation could be looked at to include the number of Accepted PQLs**

## SMB Customer Advocate

All inbound leads in the SMB sales segment are evenly routed to the SMB Customer Advocate team. Unlike the BDR team which focuses purely on inbound lead management and qualification for mid market and larger prospects and then passes qualified leads to a closing sales function, the SMB Customer Advocates handle the entire buying journey of all SMB customers. This includes inbound lead qualification, closing, renewing, and expanding SMB customer accounts.

The goal of the SMB Customer Advocate team is to support SMB prospects and customers with whatever they need, letting our customers drive the buying process rather than trying to encourage our prospects to conform to our sales process. To that end, SMB customer advocates offer a great product, with fair terms, at a great price, but they do not negotiate on contract terms or price. Their are purely focused on doing what is right for their customers. Does the customer want to buy through our web store? Great! The SMB Customer Advocate team will help with any questions that come up before, during, or after web purchase. Would the customer rather be invoiced? No problem! The SMB Customer Advocate team is there to demo the product, assist with a trial, and answer any pre-sales questions that come up. In addition, the SMB Customer Advocate will assist customers as needed, and will be the customer's point of contact for any renewal and expansion discussions.

## How to SDR

[Video: Welcome to the GitLab Sales Development Team](https://www.youtube.com/watch?v=ESZtmKUn9QY&feature=youtu.be)

As an Outbound Sales Development Representative (SDR), you will be responsible for one of the most difficult and important parts of growing a business: outbound prospecting. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities within our large account segment.

Getting started, you'll work closely with your Strategic Account Leader (SAL) to strategize which accounts to focus on as well as the potential entry points into those accounts. From there you will work on your own to further research and understanding the account, their business, their initiatives and other related details. Next you will hunt for and identify prospects that have a role in making software development tooling decisions as well as the various avenues through which you could communicate with these prospects. Utilizing all of this research you’ll then strategically communicate GitLab's value with your prospects, as well as the value of taking a meeting with GitLab for a product overview.


### SDR Expectations

[Video: SDR Expectations & Metrics](https://www.youtube.com/watch?v=h2zd8BYQwhM)
* Meet monthly quota of Sales Accepted Opportunities (SAOs)
* Exceptional Salesforce hygiene, logging all prospecting activity, opportunity creation
* Maintaining a high sense of autonomy to focus on what's most important; created and sales accepted opportunities
* Attendance in each initial qualifying meeting created - documenting notes and communicating with your SAL after the meeting
  - It will be in your best interest to sit in on as many meetings as possible at different stages in the buying process to see how the SAL's work with prospects beyond qualifying. The better you understand the SALs you work with, the better you will be at making them successful through outbound prospecting support.

### Working with Account Executives

Strategic Account Executives (SALs) receive support from the Outbound SDR team. Meeting cadence consist of the following:

- **Initial kick-off meeting**: Time - 1 hour; Discuss strategy, accounts, and schedules
- **Weekly Status Meeting**: Time - 30 minutes; Discuss initial meetings, opportunities, and prospecting strategy
- **Monthly Recurring Strategy Meeting**: Time - 1 hour; Evaluate strategy and opportunities

Additional ad-hoc meetings may be scheduled in addition to the above.

Strategic Account Leaders (SALs) have direct impact on the Outbound SDR's success. It is very important to form a strong partnership to as you begin your prospecting efforts. The SALs you work with will know a lot about the accounts you are prospecting into, so be sure to communicate regularly to ensure you are as effective as possible.

### Preparing for Field Events

Book face to face meetings at [Field Events](/handbook/marketing/marketing-sales-development/field-marketing/#event-outreach)

## SDR Compensation and Quota

SDR’s total compensation is based on two key metrics:
* Sales Accepted Opportunities (SAOs)
* Closed won business from SAOs - 1% commission for any closed won opportunity produced, **so as long as the rep is employed as a SDR by GitLab.**

Quota is based solely on SAOs. Think of the 1% commission on opportunities you source that a sales person subsequently closes and wins as an added bonus. To be paid on your closed won business, your activity should clearly depict that your prospecting work sourced the opportunity.

SDRs will be expected to get 2 SAOs within the first 28 days of employment, 3 SAOs by the end of their second month, 5 by month 3, and 6 by month 4. After month 4, SDRs will be considered fully ramped and expected to generate 8 SAOs per month.

There is an accelerator for SDRs who deliver over their SAO quota. The accelerator only applies to SAOs; the 1% commission on closed won business is not eligible. Your commission is calculated as follows:

* Tier 1 - Base rate: Per accepted opportunity commission is paid at a base rate of your monthly variable on target earnings divided by your monthly target.
* Tier 2 - Accelerator from 100% to 200% achievement: Each accepted opportunity after initial target is achieved will be paid at 1.05 times the base rate.
* Tier 3 - Accelerator from 200% achievement onwards: After 200% quota achievement each opportunity after will be paid at 1.1 times the base rate.
* There is no floor (except for 200% for ramping reps)
* Payment tiers are progressive.

Below are reports from Salesforce that GitLab leadership uses to see all the Outbound created and sales accepted opportunities. If you believe an opportunity you sourced is not reflected in the reporting, notify your manager.

* [Outbound SDR created opportunities](https://na34.salesforce.com/00O61000003nmhe)
* [Outbound SDR sales accepted opportunities](https://na34.salesforce.com/00O61000003nmhU?dbw=1)

**SDRs will also be measured on the following:**

* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent

The above measurements do not impact your quota attainment, but are critical to being a successful SDR. A SDRs main focus should always be achieving their SAO quota.

## SDR Account Allocation
* The SDR Team Lead will assign 25-30 accounts per quarter in Salesforce from the Large segments to a SDR. These accounts will be provided in close partnership with a Regional Sales Director. The accounts will be identified and selected as the best accounts to focus on in order to meet our revenue objectives.
* The accounts will be a mixture of Existing Customer accounts with small GitLab Licenses, Community Edition Accounts, and Accounts that are currently not using GitLab  

To add an SDR to an Account, complete the following steps:
1. Navigate to the ACCOUNT record, click `edit`
2. Go to the **`GitLab Team`** section
3. Select the SDR in the picklist field
4. Save the record

If you need to mass reassign or mass remove an SDR from Accounts, please contact Sales Operations. Most reassignments will be handled during on- or off-boarding process. 

## SDR Account Research
* Now that you have a book of business, the next step is determining which accounts and roles at those companies you should approach first. This can be determined in conjunction with your assigned account executive or SDR Manager. Chances are they may have prospected into the account in the past or have some telling information about the account. Weekly syncs on assigned accounts will directly impact your success as an SDR. To be prepared for that weekly sync and your everyday prospecting it helps to do the following:
 * Create a targeted list; look for key indicators like number of existing contacts, historical opportunities, CE usage, and EE customers
 * Within that list, identify the buyers based on GitLab's buyer personas, keep the list short and simple for each account
 * Update your chosen target accounts in Salesforce by looking for any missing data points such as phone numbers, emails, LinkedIn profiles, contacts etc.
 * Look for contacts with IT leadership and application development leadership responsibilities. This can be senior technical individual contributors such as software architects, or leaders in the organization's IT department all the way up to a CIO or CTO.
 * Filter in, filter out. Alternate your target accounts from your main 30 accounts to keep you going.

Leveraging the data gathered above, a SDR should spend no more than three minutes performing account research to personalize messaging. Below is an outline to help you come up with a personalized message in three minutes or less

**Personalized message research help and example:**

Three things in three minutes about the Organization, Industry, Prospect

**Salesforce**
  * Locate the account in Salesforce
  * Are there other leads that are being worked?
  * Is this a customer account?
  * Are there any open or closed opportunities? Are we able to reference these?
  * Activity history
  * Who in the organization have we been in contact with? Can we follow up?

**LinkedIn**
  * Refer to the prospect's summary. Is there any mutual interest you share?
  * Does anything stand out that is relevant to their needs as an organization?
  * What is the prospect's role and how does that affect your messaging?
  * Have they published any articles that would be work referencing?
  * Work history, any GitLab customers that we can reference?
  * Personalization: Any mutual Hobbies/Interests you share? Location? Education?
  * Connections, are they connected to anyone at GitLab for a possible introduction? If so, please slack that individual to see if they would be willing to make a introduction

**Company Website**
  * Mission statement
  * Press releases/Newsroom
  * Major IT Initiatives
  * (ctrl F) search for keywords
  * Navigate to "Jobs" page and look for job description for developers, look what tools they require to hire a developer, this will give you and idea of how their tool stack can look like.

## Outbound Prospecting - The Outreach
At GitLab, the top performing SDRs consistently make and send out a high volume of phone calls and personalized emails in their prospecting efforts. Social Selling and general creativity around prospecting activities is highly encouraged and often leads to more success. In example; SDRs will all be provided a license to Vidyard [(download it here)](https://www.vidyard.com/govideo/).

In the spirit of GitLab's vision: "Everyone can contribute", GitLab SDR's will be expected lead, participate and contribute in regular discussions around new creative ways to engage our prospective customers. When calling into Europe, be mindful of GDPR and only email contacts whose `GDPR Compliant` boxes are checked in SFDC; Also, all Discover.Org contacts are GDPR opt in.

[Video: Sample Vidyard Video for a prospect](https://share.vidyard.com/watch/HyoPfVvUJtpduNSXYtbQgP)

[Video: Best Practices for Opening Calls](https://www.youtube.com/watch?v=jR1RGSBHo5Q&feature=youtu.be)

[Video: Best Practices for Asking Questions](https://www.youtube.com/watch?v=Lv4nozScm-M)

### Cold Calling

With all the technology available to you as a SDR here at GitLab there really isn't such a thing as a "cold" call. Below are a few important tips that will help you structure a prospecting call:
* Call about them, not you. Don't be what everyone thinks a salesperson is, don't throw up on them with cool information about GitLab
* Always have their LinkedIn profile up
* Listen. Provide value for them to continue conversations with GitLab
* Ensure that you are calling at the right times. Data suggests in the morning, at lunch, and in the evening as best call practices. Schedule your day accordingly.
* Be confident and passionate
* Focus on decision makers, but don't leave the other roles hanging
* Practice with other SDRs or your Team Lead on objection handling
* Obtain a commitment

### SDR Prospecting Call Structure
* Introduction
    * Immediately introduce yourself and GitLab
    * State intention to book a meeting
    * Ask for time
* Initial benefit statement
* Qualification, ask questions, listen
* Obtain the commitment, ask for the meeting, come prepared with a few times off hand that work for you and be specific

### SDR Objection Handling
[Video: Objection Handling Techniques](https://www.youtube.com/watch?v=Q_RnXedP0JI)

**Dealing with rejection over the phone**
* You will be told "no" a lot. Don't let this bother you, ask yourself after every call; "What could I have done better or differently?" Then gather yourself and approach the next call.
* Do we end the call when we hear the first objection? NO! If you have someone on the phone already don't lose the chance to have a conversation. Below is a guide for navigating objections in prospecting calls called the *3 No Rule*

**3 No Rule**

**A- Approach/Intro**
* Hi (prospect's name), this is (your name) calling with GitLab
    * ...I know you weren't expecting my call... so I will be brief OR ...do you have a few minutes?
    * ...Did I catch you at an okay time?
    * ...have you hear of GitLab?

**B- Brief Initial Benefit Statement (The Why)**
* I recently (read/saw/spoke to/understand) that (something you found in your pre call research)...
    * The purpose of my call is...
    * ...While I have you on the phone, I had a couple of quick questions...

**C- Connect/Ask Questions**
* Have you heard of GitLab?
    * "No" - Tell them
    * "Yes"
        * Where did you hear about it?
        * Have you used it before?
    * What are you using for... (topic of your initial benefit statement)?
    * What do you like about X tool?
    * Anything you would change about X tool if you could?

**D- Why Now?**
* After you have found an area to add value to:
    * Give initial benefit statement (focusing on how we can help them, give examples, share case studies)
    * Recommend next steps based on identified need, pain, and goals
    * Be confident, assumptive, and consultative

**E- Obtaining the commitment** - [Video: Best Practices for Closing for Appointments](https://www.youtube.com/watch?v=FfUfmguwki4&feature=youtu.be)
* Meeting first - If they object, bring up the need, pain and goals that you have identified, reposition yourself and ask again if they could get X value, would it be worth a 5-10 minute call?
* Why not?
    * if still a "no" suggest they attend or watch a recorded webinar on our product
        * If still a "no", recommend a case study or article. Anything that you can follow up with them on

**F- Follow up**
* Send a recap email with what you told them you would send
* Record the notes about your conversation in Salesforce and set tasks for future follow up
