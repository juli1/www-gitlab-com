---
layout: markdown_page
title: "Incentives at GitLab"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

---

The following incentives are available for GitLabbers. Also see our separate page on [benefits](/handbook/benefits/) available to GitLabbers.

### IACV Target Dinner Evangelism Reward

Since reaching our IACV targets is a team effort that integrates everything from making a great product
to providing top notch customer support and everything in between, we reward **all**
GitLabbers (not just the Sales team) for every month that we reach our IACV Targets. The incentive is [100 USD](/handbook/people-operations/global-compensation/#exchange-rates)
to each team member for the purpose of evangelizing the GitLab story.  You may use the incentive at a restaurant of your choice. Enjoy!

- Every Monday the Director of Sales Ops posts the progress of Incremental Annual Contract Value (IACV) bookings against plan in the wins channel.   Director of Sales Ops also posts progress dashboard when 25%, 50%, 75% and 100% of monthly thresholds are achieved.
- At the end of each month, when bookings are finalized, Director of Sales Ops announces on the Company call the achievement of Incremental Annual Contract Value (IACV) as a percentage of plan and whether or not the sales evangelism dinner has been earned.  Sales Ops also announces the next month's target for IACV.
- To claim the incentive, please submit your receipt through Expensify or include on your contractor invoice as a reimbursable expense.
- Indicate on your receipt and in the comment section in expensify "GitLab evangelism" and the names of the other participants.
- You should spend the incentive on eating out, and can be reimbursed _up to_ the maximum of [100 USD](/handbook/people-operations/global-compensation/#exchange-rates).
- Use the incentive in the month following the announcement. So for example, if we reach our target in March, use your incentive in April.
- If you cannot, or decide not to, use the incentive in the expected month, you can carry it over to the next month by notifying [Accounts Payable](mailto:ap@gitlab.com) before the 22nd of the month (release day!). You can only carry over one month in this way.

### Discretionary Bonuses

#### Overview

1. Every now and then, individual GitLabbers really shine as they live our values.  We especially like to celebrate the special moments that exemplify the behavior we want to see in all GitLabbers.
1. We recognize this through the #thanks channel, and sometimes also through a discretionary bonus.
1. [Anybody can recommend a discretionary bonus for a Gitlabber to the GitLabber's manager](/handbook/incentives/#process-for-recommending-a-team-member-for-a-bonus-in-bamboohr) for a $1,000 at [the exchange rate](/handbook/people-operations/global-compensation/#exchange-rates).
1. We are fixing this amount at $1,000 thoughtfully and purposefully. We want the focus to be on the value and the behavior, not on the perceived monetary impact to GitLab. This is about recognition.
1. A general guideline is that 1 in 10 employees might receive a discretionary bonus each month. This can vary greatly as we don't give out bonuses just to hit a quota, nor would we hold one back because a certain number had already been awarded.
1. There is no limit to the frequency with which someone can receive a bonus. If someone deserves a bonus a day after being nominated for one we should do a second one.

#### Process for Recommending a Team Member for a Bonus in BambooHR

**Any GitLabber**

1. Write a 1 sentence description of how the GitLabber has demonstrated a specific GitLab value in their work. 
1. Email or Slack that sentence to the GitLabber's manager, suggesting a discretionary bonus, and set-up a 15 minute Zoom meeting to discuss your proposal.
1. Remember that the manager may or may not agree and has full discretion (with approval of their manager) on whether that person gets a bonus.  Don't forget that you can also use the #thanks channel to recognize people, as well.

**Manager Process**

1. Login to BambooHR.
1. Select the team member you would like to give the discretionary bonus to.
1. In the top right hand corner, click Request a Change.
1. Select Discretionary Bonus.
1. Enter in all applicable fields in the form, and then submit.
1. In the Comments to Approvers field in the request in BambooHR, include a 1-sentence justification that details how the GitLabber demonstrated one or more of our [values](/handbook/values) in this specific instance.
1. Submit the bonus request to your manager for approval.
1. Once approved, you must copy and paste the 1-sentence justification into the GitLab Company Call agenda and recognize the GitLabber on the company call. Remember, a discretionary bonus is for recognition so make sure to take this final step.  The Company Call agend is the public record of how people are living up to the GitLab values every day.

**Approval Process:**

1. The Manager's Manager receives an alert from BambooHR and can approve or deny.
1. The request is then sent to the People Ops Admins for final approval and to confirm that there is a 1-sentence justification comment in the Bamboo record.
1. Once approved, the request is sent to the People Ops Analyst to process the bonus in the applicable payroll and notify the manager via email.
1. Notify Payroll of the bonus. 
1. Per the email from People Ops, the manager will notify the team member of the bonus and announce on the company call. The manager announces the “who” and “why” of the bonus. The team agenda item should include the 1-sentence justification of the bonus.


#### Top Team Member

1. A Top Team Member (T2M) exemplifies the [GitLab Values](/handbook/values/) beyond of the [team of people](/company/team/structure/) they directly work with.
1. Each quarter People Ops will request managers to nominate a team member who is not on their own team to receive the T2M award. All team members can submit nominations, but just ping your manager in the issue to upvote.
  * The request will be sent on the 5th of month and nominations will close at the end of the day (EST) on the 10th. The prize will be awarded on the 15th or the following business day (if on a weekend). The People Ops Analyst will tally the nominations. The decision will be finalized by a review committee made up of the Chief Culture Officer, CEO, CTO and the VP of Product. The executive team member of the winner will confirm the award to ensure there are no pending performance issues.
  * Nominations should focus on efforts and explain specific situations that represent any of our [values](/handbook/values/).
  * The T2M award will be announced on the [Company Call](/handbook/communication/#company-call) to recognize the team member and posted on the culture page.
1. The award will be an item from our GitLab Store.
1. Takeaway from this reward program: reward the cross-functional collaboration behavior that we want to encourage.

#### GitLab Anniversaries

To celebrate a GitLab hire date anniversary, People Ops will send a shout out on the `#celebrations` channel on Slack. 

### Real Examples of Real Team Members Who Received Bonuses for Doing Great Things

* This document presents the case for awarding a UX team member an incentive. The UX team member is reliable, fair and respectful, consistently acting in the best interest of the company as well as the team.
  * Collaboration: The UX team member took on the extra duties of UX Lead and handled the interim duties seamlessly. She responded kindly to the community feedback on sidebar issue in 9.0 well. She personally helped the VP of Engineering finish the merit review process for the UX team.
  * The UX team member has greatly helped the UX Lead transition to her new role by assisting with meetings, transferring knowledge openly, and being available for questions whenever necessary.
  * Results and Efficiency: The UX team member quickly delivered screenshots for a partnership in a day or two. She did a great job with the UX team updates, providing clear and visual screenshots of what the team was working on. She helped the team deliver the UX improvements shown in those updates.

* A Support team member received a bonus for:
  * Results & boring solutions: He managed to swap the database from PG9.2 to PG9.6 without significant downtime. It was even boring. ­
  * Sharing: His issues, guidelines, monitoring, you­name­it are exemplary. He keeps raising the bar and leaving a written trace to follow when he is not around. ­
  * Efficiency: He always hits the nail and does the right thing, has a great sense of priorities and can jump into production to solve a _right now_ pain in a heartbeat. ­
  * Quirkiness: What to say? Do you want someone washing grapes or painting a wall in a call, just invite him.

* A Marketing team member received a bonus for:
  * Transparency: The marketing team member always works in the open. In our 1:1s she is very clear on her focus and aligns priorities with team priorities. Every thing she is working on links to an issue.
  * Efficiency: The marketing team member is an excellent example of someone who can get multiple things done in a short amount of time. She can efficiently manage many high quality projects without getting bogged down in the details.
  * Collaboration: The marketing team member worked with the VP of Scaling to update the general handbook to make it prettier. This shows she collaborates well outside of her functional group. The marketing team member has also been helping a colleague with content management.
  * Directness: The marketing team member gives excellent review feedback on blog posts. She is very direct and not afraid of perfection.

* A Product team member received a bonus for:
  * Collaboration:  Works together well with everyone and actively recruits opinions across the organization.
  * Results: Shipping consistent and meaningful improvements in issues, board, etc.
  * Efficiency: Actively avoids meetings and encourages async work.
  * Iteration: Reduces everything to its very minimal iteration, not paying with quality or usability, yet moving forward with each release.

### Referral Bonuses

Chances are that if you work at GitLab, you have great friends and peers who would
also be fantastic additions to our [Team](/company/team/) and who
may be interested in one of the current [Job Openings](/jobs/).
To help us grow the team with exceptional people, we have referral bonuses that work as follows:

1. We want to encourage and support [diversity](/handbook/values) and [frugality](/handbook/values) on our team and in our hiring practices, so we will offer the following referral bonuses once the new team member has been with the company for 3 months.
    * Any great candidate that is referred and hired will earn a GitLab employee a [$1,000](/handbook/people-operations/global-compensation/#exchange-rates) bonus once the new team member has been with the company for 3 months.
    * We will offer a [$1500](/handbook/people-operations/global-compensation/#exchange-rates) referral bonus for hires from a Location factor less than 0.7.
    * We will offer a [$2000](/handbook/people-operations/global-compensation/#exchange-rates) referral bonus for hires to teams with ["underrepresented groups"](http://seldo.com/weblog/2014/06/25/a_comparison_of_diversity_at_three_major_tech_companies). "Underrepresented groups" may refer to women, people of color, Hispanic/Latino people, veterans, or others, but it may also be defined differently from department to department and is subject to change as those departments grows more diverse. People Ops will confirm the bonus amount when it is entered into BambooHR.
    * If the new hire is from an underrepresented group as defined above, the higher amount will be paid to the team member for the referral.

_Exceptions: no bonuses for hiring people who report to you, and no bonus for the executive team (VP and above)._

* Job of the Week: Each week, the recruiting team will highlight a "Job of the Week" on the company call and ask for our Team of GitLabbers
to reach out to their networks and refer people they know who would be a great fit, skillset-wise and culturally.
* Please read about [how to make a referral](/handbook/hiring/greenhouse/#making-a-referral) for more information.

People Ops will process the bonus.

#### Document a future bonus in BambooHR

1. Go to the employee who referred the new team member in BambooHR
1. Under the "Jobs" tab
1. Under the "Bonus" table, click "Add Bonus"
1. The bonus date is 3 months from the new team member's start date
1. Enter the bonus amount, dependent on above
1. Enter the bonus type to be a Referral Bonus
1. Enter a note stating that this is a future bonus (this will be changed once the bonus has been paid)

#### Notification to Process a Bonus

BambooHR will send an email to PeopleOps on the date that the referral bonus should be paid for processing through the applicable payroll system. If the team member is a contractor, send them an email (and cc Finance) to invoice the bonus.
Once the bonus has been processed, change the note in BambooHR to denote the referral bonus has been paid.

### Visiting grant

GitLab is an [all-remote company](http://www.remoteonly.org/) with GitLabbers all over the world (see the map on our [Team page](/company/team/). If you want to visit other team members to get to know them, GitLab will assist with travel expenses, up to a total of $150 per team member that you visit and work with.  This includes things such as; **flights, trains, and transportation to and from the airport**. You don't need to work on the same project or team, either, so long as you discuss work for at least part of the time you're meeting. Before booking any travel using the travel grant budget please discuss your proposed travel with your manager. We encourage team members to utilize the travel grant, however in some cases i.e. a team member has  performance issues related to their role the visiting grant would not be applicable.

Note that lodging, meals, and local travel while visiting are not typically covered for anyone as that wouldn't really be fair to those being visited. It may be acceptable to cover a meal, however, if the meeting is pre-announced to other people in the region to encourage as many of them to attend as possible and there are four or more GitLabbers attending.

There are many regular meet-ups of GitLabbers in many cities. We have a [shared calendar][meetup-calendar] to view the schedule. Subscribe to it, make use of the visiting grant, and join meet-ups near you (or far away!).

Douwe and Robert took advantage of the visiting grant when they traveled to [49 different colleagues in 20 cities, in 14 countries, on five continents, in 6 months](/2017/01/31/around-the-world-in-6-releases/). Inspired by them, Dimitrie went on a similar journey and provided a [great template](https://docs.google.com/spreadsheets/d/1Cwi2PfO1HeDm8Lap9J2tPmfYFaC8AmmKQg1fCmtdGI4/edit?usp=sharing). Also, check out [tips on working remotely while abroad](/company/culture/all-remote/#tips-on-working-remotely-while-abroad).

To claim the grant, include a line item on your expense report or invoice of your flights, trains, and/or transportation to and from the airport with a list of the team members you visited. The expense report may be submitted during the first month of travel or up to 3 months after your trip has concluded. That said, if it's more [frugal](/handbook/values/#efficiency) to book and expense your travel further in advance, please do so.

[meetup-calendar]: https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cWZnajRuMm9nZHMydmlhMDFrb3ZoaGpub0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t

### GitLab Gold

Every GitLab employee can request the [Gold](/pricing/#gitlab-com) tier for GitLab.com.
In case an employee has separate private and work accounts on GitLab.com, they can request it for both. This incentive **does not** apply to groups owned by GitLab employees (Group-level Gold features such as epics will not be available for Gold GitLabber personal accounts, for instance).

In order to request this benefit please open an issue in the [internal services issue tracker](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=gitlabber_gold_request&issue[title]=GitLab.com%20Gold%20tier%20request:%20@username).
Make sure to adjust `@username` in the issue title.
