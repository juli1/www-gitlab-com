---
layout: markdown_page
title: "Data Team"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> Quick Links
[Primary Project](https://gitlab.com/gitlab-data/analytics/){:.btn .btn-purple-inv}

[Periscope Directory](/handbook/business-ops/data-team/periscope-directory/index.html){:.btn .btn-purple-inv}

[dbt docs](https://gitlab-data.gitlab.io/analytics/dbt/snowflake/#!/overview){:.btn .btn-purple-inv}

[SQL Style Guide](/handbook/business-ops/data-team/sql-style-guide){:.btn .btn-purple-inv}

[Python Style Guide](/handbook/business-ops/data-team/python-style-guide){:.btn .btn-purple-inv}
## <i class="fas fa-map-signs fa-fw icon-color font-awesome" aria-hidden="true"></i> Roadmap

[Epics](https://gitlab.com/groups/gitlab-data/-/roadmap?layout=MONTHS&sort=start_date_asc){:.btn .btn-purple}

[OKRs](https://about.gitlab.com/company/okrs/){:.btn .btn-purple}

----

## GET IN TOUCH

* [Data Team Project](https://gitlab.com/gitlab-data/analytics/)
* #analytics on slack

<br />

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We <i class="fab fa-gitlab orange font-awesome" aria-hidden="true"></i> Data</strong>
  </p>
</div>
{:.no_toc}

## Data Team Principles

The Data Team at GitLab is working to establish a world-class analytics function by utilizing the tools of DevOps in combination with the core values of GitLab. We believe that data teams have much to learn from DevOps. We will work to model good software development best practices and integrate them into our data management and analytics. 

A typical data team has members who fall along a spectrum of skills and focus. For now, the analytics function at GitLab has Data Engineers and Data Analysts; eventually the team will include Data Scientists. Analysts are divided into being part of the Core Data Function and specializing in different functions in the company.

Data Engineers on our team are essentially software engineers who have a particular focus on data movement and orchestration. The transition to DevOps is typically easier for them because much of their work is done using the command line and scripting languages such as Bash and Python. One challenge in particular are data pipelines. Most pipelines are not well tested, data movement is not typically idempotent, and auditability of history is challenging. 

Data Analysts are further from DevOps practices than Data Engineers. Most analysts use SQL for their analytics and queries, with Python or R a close second. In the past, data queries and transformations may have been done by custom tooling or software written by other companies. These tools and approaches share similar traits in that they're likely not version controlled, there are probably few tests around them, and they are difficult to maintain at scale. 

Data Scientists are probably furthest from integrating DevOps practices into their work. Much of their work is done in tools like Jupyter Notebooks or R Studio. Those who do machine learning create models that are not typically version controlled. Data management and accessibility is also a concern as well. 

We will work closely with the analytics community to find solutions to these challenges. Some of the solutions may be cultural in nature, and we aim to be a model for other organizations of how a world-class Data and Analytics team can utilize the best of DevOps for all Data Operations.

Some of our beliefs are:

* Everything can and should be defined in code.
* Everything can and should be version controlled.
* Data Engineers, Data Analysts, and Data Scientists can and should integrate best practices from DevOps into their workflow.
* It is possible to serve the business while having a high-quality, maintainable code base.
* Analytics, and the code that supports it, can and should be open source.
* There can be a single source of truth for every analytic question within a company.
* Data team managers serve their team and not themselves.

## <i class="fas fa-chart-line fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Analysis Process

Analysis usually begins with a question. A stakeholder will ask a question of the data team by creating an issue in the [Data Team project](https://gitlab.com/gitlab-data/analytics/) using the appropriate template. The analyst assigned to the project may schedule a discussion with the stakeholder(s) to further understand the needs of the analysis. This meeting will allow for analysts to understand the overall goals of the analysis, not just the singular question being asked, and should be recorded. Analysts looking for some place to start the discussion can start by asking: 
* How can your favorite reports be improved?
* How do you use this data to make decisions?
* What decisions do you make and what information will help you to make them quicker/better?

An analyst will then update the issue to reflect their understanding of the project at hand. This may mean turning an existing issue into a meta issue or an epic. Stakeholders are encouraged to engage on the appropriate issues. The issue then becomes the SSOT for the status of the project, indicating the milestone to which its been assigned and the analyst working on it, among other things. Barring any confidentiality concerns, the issue is also where the final project will be delivered. On delivery, the data team  manager will be cc'ed where s/he will provide feedback and/or request changes. When satisfied, s/he will close the issue. If the stakeholder would like to request a change after the issue has been closed, s/he should create a new issue and link to the closed issue. 

The Data Team can be found in the #analytics channel on slack.

## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i> How we Work

The data team currently works in two-week intervals, called milestones. Milestones start on Tuesdays and end on Mondays. This discourages last-minute merging on Fridays and allows the team to have milestone planning meetings at the top of the milestone.

Milestones may be three weeks long if they cover a major holiday or if the majority of the team is on vacation. As work is assigned to a person and a milestone, it gets a weight assigned to it. 

### Milestone Planning

Data Team OKRs are derived from the higher level BizOps/Finance OKRs as well as the needs of the team. At the beginning of a FQ, the team will outline all actions that are required to succeed with our KRs *and* in helping other teams measure the success of their KRs. The best way to do that is via a team brain dump session in which everyone lays out all the steps they anticipate for each of the relevant actions. This is a great time for the team to raise any blockers or concerns they foresee. These should be recorded for future reference.

These OKRs drive ~60% of the work that the core data team does. The remaining time is divided between urgent issues that come up and ad hoc/exploratory analyses. Specialty data analysts (who have the title "Data Analyst, Specialty") should have a similar break down of planned work to responsive work, but their priorities are set by their specialty manager.

Given the priorities of the team, work is then pulled into a milestone and then assigned appropriately. (Work is not assigned and then managed into a milestone.) Given the power of the [Ivy Lee](https://jamesclear.com/ivy-lee) method, this allows the team to collectively work on priorities as opposed to creating a backlog for any given person. As a tradeoff, this also means that every time a core analyst is introduced to a new data source their velocity may temporarily decrease as they come up to speed; the overall benefit to the organization that any analyst can pick up any issue will compensate for this, though.

Milestone planning should take into consideration:
* vacation timelines
* conference schedules
* team member availability
* team member work preferences (specialties are different from preferences)

Anyone on the team can wear the milestone [planning hat](/handbook/business-ops/#bizops-planning-hat); this responsibility does not have to belong to the Manager.

The timeline for milestone planning is as follows:
* T-7 (Tuesday) - Milestone Pointing meeting
   * Issues have been assigned to the milestone, but not to an individual, except where required.
   * Issues are pointed collectively in the [Milestone Planning Meeting](https://docs.google.com/document/d/1rSHFQfVqE2qgVoBhNAj3TgPnRYsBIG-Ilezghf9-pZc/edit) which now takes place one week before the milestone begins. 
   * Pointing is done without knowledge of who make pick up the task.
* T-4 (Friday) - Milestone is roughly final
   * Issues have been distributed to team members, with the appropriate considerations and preferences.
* T+0 (Tuesday)- Milestone begins; no meeting required.
* T+7 (Tuesday)- Midway point
   * Any issues that are at risk of slipping from the milestone must be raised by the assignee
* T+10 (Friday)- The last day to submit MRs for review
   * MRs must include documentation and testing to be ready to merge
   * No MRs are to be merged on Fridays
* T+13 (Mondays) - Ready MRs can be merged

The short-term goal of this process is to improve our ability to plan and estimate work through better understanding of our velocity.

### Issue Types

There are three *general* types of issues:
* Discovery 
* Introducing a new data source
* Work

Not all issues will fall into one of these buckets but 85% should. 

##### Discovery issues
Some issues may need a discovery period to understand requirements, gather feedback, or explore the work that needs to be done.
Discovery issues are usually 2 points.

##### Introducing a new data source
Introducing a new data source requires a *heavy lift* of understanding that new data source, mapping field names to logic, documenting those, and understanding what issues are being delivered. 
Usually introducing a new data source is coupled with replicating an existing dashboard from the other data source. 
This helps verify that numbers are accurate and the original data source and the data team's analysis are using the same definitions.

##### Work
This umbrella term helps capture:
* inbound requests from GitLabbers that usually materialize into a dashboard
* housekeeping improvements/technical debt from the data team
* goals of the data team
* documentation notes

It is the responsibility of the assignee to be clear on what the scope of their issue is. 
A well-defined issue has a clearly outlined problem statement. Complex or new issues may also include an outline (not all encompassing list) of what steps need to be taken. 
If an issue is not well-scoped as its assigned, it is the responsibility of the assignee to understand how to scope that issue properly and approach the appropriate team members for guidance early in the milestone.

### Issue Pointing

* Refer to the table below for point values and what they represent. 
* We size and point issues as a group. Size is about the complexity of the problem and not its complexity in relation to whom is expected to complete the task.
* Effective pointing requires more fleshed out issues, but that requirement shouldn't keep people from creating issues.
* When pointing work that happens outside of the Data Team projects, add points to the issue in the relevant Data Team project and ensure issues are cross-linked.

| Weight | Description |
| ------ | ------ |
| Null | Meta and Discussions that don't result in an MR |
| 0 | Should not be used. |
| 1 | The simplest possible change including documentation changes. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. | 
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. | 
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. | 
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. | 
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. | 

### Issue Labeling

Think of each of these groups of labels as ways of bucketing the work done. All issues should get the following classes of labels assigned to them:

* Who (Purple): Team for which work is primarily for (Data, Finance, Sales, etc.)
* What - Data or Tool
  * Data (Light Green): Data being touched (Salesforce, Zuora, Zendesk, Gitlab.com, etc.)
  * Tool (Light Blue) (Looker, dbt, Stitch, Airflow, etc.) 
* Where (Brown): Stage of data lifecycle (Infrastructure, Extract/Load, Transform, Reporting)
* When (Varies): Priority (P1-4 See table below, Backlog, 2019-Q3, etc.) 
* How (Orange): Type of work (Documentation, Break-fix, Enhancement, Refactor, Testing, Housekeeping)

Optional labels that are useful to communicate state or other priority
* State (Red) (Won't Do, Blocked, Needs Consensus, etc.)


| Priority | Description | Probability of shipping in milestone | 
| ------ | ------ | ------ |
| P1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a milestone and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| P2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| P3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| P4 | **Low**: stretch issues that are acceptable to postpone into a future milestone. | ~25% | 

### Daily Standup

Members of the data team use Geekbot for our daily standups. These are posted in [#data-daily](https://gitlab.slack.com/archives/CGG0VRJJ0/p1553619142000700). When Geekbot asks, "What are you planning on working on today? Any blockers?" try answering with specific details, so that teammates can proactively unblock you. Instead of "working on Salesforce stuff", consider "Adding Opportunity Owners for the `sfdc_opportunity_xf` model`." There is no pressure to respond to Geekbot as soon as it messages you.  Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted.

### Merge Request Workflow

*Ideally*, your workflow should be as follows:
1. Create an issue or open an existing issue.
1. Add appropriate labels to the issue issue (see above)
1. Open an MR from the issue using the "Create merge request" button. This automatically creates a unique branch based on the issue name. This marks the issue for closure once the MR is merged.
1. Push your work to the branch
1. Run any relevant jobs to the work being proposed
  * e.g. if you're working on dbt changes, run the dbt MR job and the dbt test job.
1. Document in the MR description what the purpose of the MR is, any additional changes that need to happen for the MR to be valid, and if it's a complicated MR, how you verified that the change works. See [this MR](https://gitlab.com/gitlab-data/analytics/merge_requests/658) for an example of good documentation. The goal is to make it easier for reviewers to understand what the MR is doing so it's as easy as possible to review.
1. Assign the MR to a peer to have it reviewed. If assigning to someone who can merge, either leave a comment asking for a review without merge, or you can simply leave the `WIP:` label.
  * Note that assigning someone an MR means action is required from them. 
  * Adding someone as an approver is a way to tag them for an FYI. This is similar to doing `cc @user` in a comment. 
1. Once it's ready for further review and merging, remove the `WIP:` label, mark the branch for deletion, mark squash commits, and assign to the project's maintainer. Ensure that the attached issue is appropriately labeled and pointed.

Other tips:
* If, for some reason, the merge request is closed but not Merged, you have to run the review stop job manually. Closing the MR will not trigger it. 
* If you're on a review instance of the database and you need to test a change to the SFDC snapshot, truncate the table first.
* Reviewers should have 48 hours to complete a review, so plan ahead with the end of the milestone. 
* When possible, questions/problems should be discussed with your reviewer before MR time. MR time is by definition the worst possible time to have to make meaningful changes to your models, because you’ve already done all of the work! 

## Our Data Stack

We use GitLab to operate and manage the analytics function. Everything starts with an issue. Changes are implemented via merge requests, including changes to our pipelines, extraction, loading, transformations, and parts of our analytics.

|Stage|Tool|
|:-|:-:|
|Extraction|Stitch and Custom|
|Loading|Stitch and Custom|
|Orchestration|Airflow and GitLab CI|
|Storage|Cloud SQL (PostgreSQL) and Snowflake|
|Transformations|dbt and Python scripts|
|Analysis| TBD |

## <i class="fas fa-exchange-alt fa-fw icon-color font-awesome" aria-hidden="true"></i> Extract and Load

We currently use Stitch for most of our data sources. 

| Data Source       | Pipeline  | Management Responsibility | Frequency |
|-------------------|-----------|-----------------|-----------------|
| Clearbit |     |    |   |
| CloudSQL Postgres | Stitch    | Data Team   |   |
| DiscoverOrg |     |    |   |
| Gitter            |           |           |   |
| GitLab dot Com         |  |    |   |
| SheetLoad         | SheetLoad | Data Team   |   |
| Marketo           | Stitch    | Data Team   |   12 hour intervals - Backfilled from January 1, 2013|
| Netsuite          | Stitch    | Data Team   |   30 minute intervals - Backfilled from January 1, 2013|
| Pings             | Stitch/Custom   | Data Team   | | 
| SFDC              | Stitch    | Data Team   |   1 hour intervals - Backfilled from January 1, 2013|
| Snowplow          |           | Data Team   | |
| Zendesk           | Stitch    | Data Team   |   1 hour intervals - Backfilled from January 1, 2013|
| Zuora             | Stitch    | Data Team   |   30 minute intervals - Backfilled from January 1, 2013|

Planned:
* AWS Billing
* BambooHR
* Fastly Billing
* GCP Billing
* Greenhouse
* Stripe

### SLAs by Data Source

This is the lag between real-time and the analysis displayed in the [data visualization tool](#-visualization).

* Snowplow - 1 day
* Pings - 1 day
* Gitlab.com - 1 day
* SFDC - 8 hours
* Zuora - 8 hours
* Zendesk - 8 hours
* Netsuite - 8 hours


### Adding new Data Sources

Process for adding a new data source:
* Create a new issue in the Analytics repo requesting for the data source to be added:
  * Document what tables and fields are required
  * Document the questions that this data will help answer
* Create an issue in the [Security project](https://gitlab.com/gitlab-com/gl-security/engineering/issues/) and cross-link to the Analytics issue.
  * Tag the Security team `gitlab-com/gl-security`


### Using SheetLoad

SheetLoad is the process by which a GoogleSheet, local CSV or file from GCS can be ingested into the data warehouse. This is not an ideal solution to get data into the warehouse, but may be the appropriate solution at times. 

As it is being iterated on often, the best place for up-to-date info on Sheetload is the [Sheetload readme](https://gitlab.com/gitlab-data/analytics/tree/master/extract/sheetload).

## <i class="fas fa-clock fa-fw icon-color font-awesome" aria-hidden="true"></i> Orchestration

We are in the process of moving from GitLab CI to Airflow.

## <i class="fas fa-database fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Warehouse

We currently use [Snowflake](https://docs.snowflake.net/manuals/index.html) as our data warehouse.

### Warehouse Access

To gain access to the data warehouse: 
* Create an issue in the [access requests project](https://gitlab.com/gitlab-com/access-requests) documenting the level of access required.
* Do not request a shared account - each account must be tied to a user.
* We loosely follow the paradigm explained in [this blog post](https://blog.fishtownanalytics.com/how-we-configure-snowflake-fc13f1eb36c4) around permissioning users.

#### Snowflake Permissions Paradigm

Goal: Mitigate risk of people having access to sensitive data. 

We currently use Meltano's Permission Bot in dry mode to help manage our user, roles, and permissions for Snowflake. Documentation on the permission bot: https://meltano.com/docs/meltano-cli.html#meltano-permissions

There are four things things we need to manage: 
* databases - who has access to which ones
* roles 
* user roles (mapped directly to users)
* warehouses

Two notes of the permission bot: 
* things that exist but aren't in the file don't lead to errors
* it does not _delete_ things (or produce those commands) when removing from the file

##### Data Storage

We currently use two databases- `raw` and `analytics`. The former is for EL'ed data; the latter is for data that is ready for analysis (or getting there). 

###### Raw

* Raw may contain sensitive data, so permissions need to be carefully controlled. 
* Data is stored in different schemas based on the source. 
* User access can be controlled by schema and tables. 

###### Analytics

* Analytics has three "prod" schemas: `analytics`, `analytics_staging`, and `analytics_sensitive`.
* Analysts' dev schemas (`emilie_scratch_analytics` and `emilie_scratch_staging`) also is in the analytics database. The dev schemas could have sensitive information, as they iterate through it.

##### Groups

The approach to permissions, then is laid out into the following groups. These groups sometimes map to roles.

This is the list of groups and their read/write permissions. 

| group | read | write |
| ------ | ------ | ------ |
| bi_reporter | analytics.analytics.* | none | 
| bi_reporter_sensitive | analytics.analytics.* + analytics.analytics_sensitive.* | none | 
| transformer_auto | raw.* + analytics.analytics*.* (for incrementalism) | analytics.analytics*.* | 
| transformer_data_team | raw.* | analytics.*_scratch.* | 
| transformer_limited | on and as-needed basis; only his/her scratch schemas | only his/her scratch schemas | 
| loader | raw.* (for incrementalism) | raw.* | 
| engineer | raw.* + analytics.* | raw.* + analytics.* | 

<div class="panel panel-success">
**Managing Roles for Snowflake**
{: .panel-heading}
<div class="panel-body">

Here are the proper steps for provisioning a new user and user role:

* Login and switch to `securityadmin` role
* Create user (`EBURKE`)
  * Create a password using https://passwordsgenerator.net/
  * Click next and fill in additional info. Make Login Name and Display name match user name (all caps).
  * Do not set any defaults.
  * Send to person using https://onetimesecret.com/
* Create role for user (`EBURKE` for example) with `sysadmin` as the parent role (this grants the role to sysadmin)
* Grant user role to new user
* Create user_scratch schema in `ANALYTICS` as `sysadmin`
  * `CREATE SCHEMA eburke_scratch;`
* Grant ownership of scratch schema to user role
  * `GRANT OWNERSHIP ON schema eburke_scratch TO ROLE eburke;`
* Document in Snowflake config.yml permissions file
</div>
</div>

#### Snowflake Compute Resources

Compute resources in Snowflake are known as "warehouses". To better track and monitor our credit consumption, we have created several warehouses depending on who is accessing the warehouse. The names of the warehouse are appended with their size (`analyst_s` for small)

* `analyst_*` - These are for Data Analysts to use when querying the database or modeling data
* `engineer_*` - These are for Data Engineers and the Manager to use when querying the database or modeling data
* `merge_request_*` - These are scoped to GitLab CI for dbt jobs within a merge request
* `transforming_*` - These are for production dbt jobs
* `reporting` - This is for the BI tool for querying
* `loading` - This is for our Extract and Load jobs

## <i class="fas fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i> Transformation

Please see the [data analyst onboarding issue template](https://gitlab.com/gitlab-data/analytics/blob/master/.gitlab/issue_templates/onboarding.md) for details on getting started with dbt. 

At times, we rely on dbt packages for some data transformation. [Package management](https://docs.getdbt.com/docs/package-management) is built-in to dbt. A full list of packages available are on the [dbt Hub site](https://hub.getdbt.com). We use the repository-syntax instead of the hub-syntax in our `packages.yml` file.

### Tips and Tricks about Working with dbt
* The goal of a (final) `_xf` dbt model should be a `BEAM*` table, which means it follows the business event analysis & model structure and answers the who, what, where, when, how many, why, and how question combinations that measure the business. 
* Model names should be as obvious as possible and should use full words where possible, e.g. `accounts` instead of `accts`.
* Documenting and testing new data models is a part of the process of creating them. A new dbt model is not complete without tests and documentation.  
* Definitions to know
   * `source table` - (can also be called `raw table`) table coming directly from data source as configured by the manifest. It is stored directly in a schema that indicates its original data source, e.g. `sfdc`
   * `base models`- the only dbt models that reference the source table; base models have minimal transformational logic (usually limited to filtering out rows with data integrity issues or actively flagged not for analysis and renaming columns for easier analysis); can be found in the `analytics_staging` schema; is used in `ref` statements by `end-user models`
   * `end-user models` - dbt models used for analysis. The final version of a model will likely be indicated with an `_xf` suffix when it’s goal is to be a `BEAM*` table. It should follow the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business. End user models are found in the `analytics` schema.

### Configuration for dbt
* Users of dbt should have their default `prod` schema set to their scratch environment. The environment variable can be set as follows:
  * `export SNOWFLAKE_TRANSFORM_SCHEMA="tmurphy_scratch"`
* The smallest possible warehouse should be stored as an evironment variable. Our dbt jobs use `SNOWFLAKE_TRANSFORM_WAREHOUSE` as the variable name to identify the warehouse. The environment variable can be set in the `.bashrc` or `.zshrc` file as follows:
  * `export SNOWFLAKE_TRANSFORM_WAREHOUSE="ANALYST_XS"`
  * In cases where more compute is required, the variable can be overwritten by adding `--vars '{warehouse_name: analyst_xl}'` to the dbt command


## <i class="fas fa-chart-bar fa-fw icon-color font-awesome" aria-hidden="true"></i> Visualization

We use [Periscope](https://www.periscopedata.com) as our Data Visualization and Business Intelligence tool. To request access, please follow submit an [access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request).

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Roles

### Data Analyst
[Position Description](/job-families/finance/data-analyst/){:.btn .btn-purple}

### Data Engineer
[Position Description](/job-families/finance/data-engineer/){:.btn .btn-purple}

### Manager
[Position Description](/job-families/finance/manager-data-and-analytics/){:.btn .btn-purple}


<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>

