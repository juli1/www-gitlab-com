---
layout: markdown_page
title: "VUL.4.01 - Enterprise Antivirus Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.4.01 - Enterprise Antivirus

## Control Statement

If applicable, GitLab has managed enterprise antivirus deployments and ensures the following:

* Signature definitions are updated
* Full scans are performed quarterly and real-time scans are enabled
* Alerts are reviewed and resolved by authorized personnel

## Context

This control outlines the components of a successfully deployed antivirus program which helps add another layer of risk mitigation to the GitLab environment. The applicability in this control is left vague since we have to apply some reason to this control. We know there are some systems that either aren't possible to install antivirus software on or systems that wouldn't have any risk reduced by installing antivirus software. The intent of this control is to install this software anywhere it is feasible to and not to only where it is convenient.

## Scope

This control applies to all GitLab production systems that can run antivirus software.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.4.01_enterprise_antivirus.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.4.01_enterprise_antivirus.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.4.01_enterprise_antivirus.md).

## Framework Mapping

* ISO
  * A.12.2.1
* SOC2 CC
  * CC6.8
  * CC7.1
* PCI
  * 5.1
  * 5.1.1
  * 5.1.2
  * 5.2
  * 6.2
