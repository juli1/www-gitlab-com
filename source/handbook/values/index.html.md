---
layout: markdown_page
title: "GitLab Values"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## CREDIT

GitLab's six values are **Collaboration**, **Results**, **Efficiency**, **Diversity**, **Iteration**, and **Transparency**, and together they spell the **CREDIT** we give each other by assuming good intent. They are made actionable below.

### About our Values

We take inspiration from other companies, and we always go for the boring solutions. Just like the rest of our work, we continually adjust our values and strive always to make them better. We used to have more values, but it was difficult to remember them all, so we condensed them and gave sub-values and created an acronym. Everyone is welcome to suggest improvements.

### Collaboration
{:.gitlab-purple}

Helping others is a priority, even when it is not immediately related to the goals that you are trying to achieve. Similarly, you can rely on others for
help and advice—in fact, you're expected to do so. Anyone can chime in on any subject, including people who don't work at GitLab. The person who's responsible for the work decides how to do it, but they should always take each suggestion seriously and try to respond and explain why it may or may not have been implemented.

##### Kindness 
{:.no_toc}
We value caring for others. Demonstrating we care for people provides an effective framework for challenging directly and delivering feedback. We disagree with companies that say [Evaluate People Accurately, Not "Kindly"](https://inside.bwater.com/publications/principles_excerpt). We're all for accurate assessment, but we think it must be done in a kind way. Give as much positive feedback as you can, and do it in a public way.

##### Share
{:.no_toc}
There are aspects of GitLab culture, such as extreme transparency, that are unintuitive to outsiders and new team members. Be willing to invest in people and engage in open dialogue. For example, consider making private issues public wherever possible so that we can all learn from the experience.

##### Negative is 1-1
{:.no_toc}
Give negative feedback in the smallest setting possible. One-on-one video calls are preferred.

##### Say thanks
{:.no_toc}
Recognize the people that helped you publicly, for example in our [#thanks chat channel](/handbook/communication/#internal-communication).

##### Give feedback effectively
{:.no_toc}
Giving feedback is challenging, but it's important to deliver it effectively. When providing feedback, always make it about the work itself, focus on the business impact and not the person. Make sure to provide at least one clear and recent example. If a person is going through a hard time in their personal life, then take that into account. An example of giving positive feedback is our [thanks chat channel](/handbook/communication/#internal-communication). For managers, it's important to realize that employees react to a negative incident with their managers [six times more strongly](https://hbr.org/2013/03/the-delicate-art-of-giving-fee) than they do to a positive one. Keeping that in mind, if an error is so inconsequential that the value gained from providing criticism is low, it might make sense to keep that feedback to yourself. In the situations where negative feedback must be given, focus on the purpose for that feedback: to improve the employee’s performance going forward. Give recognition generously, in the open, and often to [generate more engagement](http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?infotype=SA&subtype=WH&htmlfid=LOW14298USEN) from your team.

##### Get to know each other
{:.no_toc}
We use a lot of text-based communication, and if you know the person behind the text, it will be easier to prevent conflicts. So encourage people to get to know each other on a personal level through our [company calls](/handbook/communication/#company-call), [virtual coffee breaks](/company/culture/all-remote/#coffee-break-calls), and during [GitLab Contribute](/company/culture/contribute/).

##### Don't pull rank
{:.no_toc}
If you have to remind someone of the position you have in the company, you're doing something wrong. People already know [our decision-making process](https://about.gitlab.com/handbook/leadership/#making-decisions). Explain why you're making the decision, and respect everyone irrespective of their function.

##### Assume positive intent
{:.no_toc}
We naturally have a double standard when it comes to the actions of others. We blame circumstances for our own mistakes, but individuals for theirs. This double standard is called the [Fundamental Attribution Error](https://en.wikipedia.org/wiki/Fundamental_attribution_error). In order to mitigate this bias you should always [assume positive intent](https://www.collaborativeway.com/general/a-ceos-advice-assume-positive-intent/) in your interactions with others, respecting their expertise and giving them grace in the face of what you might perceive as mistakes.

##### Address behavior, but don't label people
{:.no_toc}
There is a lot of good in [this article](http://bobsutton.typepad.com/my_weblog/2006/10/the_no_asshole_.html) about not wanting jerks on our team, but we believe that **jerk** is a label for behavior rather than an inherent classification of a person.  We avoid classifications.

##### Say sorry
{:.no_toc}
If you made a mistake apologize, saying sorry is not a sign of weakness but one of strength. The people that do the most work will likely make the most mistakes. Additionally, when we share our mistakes and bring attention to them, others can learn from us, and the same mistake is less likely to repeated by someone else.

##### No ego
{:.no_toc}
Don't defend a point to win an argument or double-down on a mistake. You are not your work; you don't have to defend your point. You do have to search for the right answer with help from others.

##### See others succeed
{:.no_toc}
A candidate who has talked to a lot of people inside GitLab mentioned that, compared to other companies, one thing stood out the most: everyone here mentioned wanting to see each other succeed.

##### People are not their work
{:.no_toc}
Always make suggestions about examples of work, not the person. Say, "you didn't respond to my feedback about the design," instead of, "you never listen". And, when receiving feedback, keep in mind that feedback is the best way to improve and that others want to see you succeed.

##### Do it yourself
{:.no_toc}
Our collaboration value is about helping each other when we have questions, need critique, or need help. No need to brainstorm, wait for consensus, or [do with two what you can do yourself](https://www.inc.com/geoffrey-james/collaboration-is-the-enemy-of-innovation.html).

##### Blameless problem solving
{:.no_toc}
Investigate mistakes in a way that focuses on the situational aspects of a failure’s mechanism and the decision-making process that led to the failure rather than cast blame on a person or team. We hold blameless [root cause analyses](https://codeascraft.com/2012/05/22/blameless-postmortems/) and [retrospectives](handbook/engineering/management/team-retrospectives/) for stakeholders to speak up without fear of punishment or retribution.

##### Dogfooding
{:.no_toc}
We [use our own product](https://en.wikipedia.org/wiki/Eating_your_own_dog_food). Our development organization uses GitLab.com to manage the DevOps lifecycle of the GitLab CE and EE projects. Our entire company uses GitLab to collaborate on this handbook. We also capture other content and processes in Git repos and manage them with GitLab.

### Results
{:.gitlab-purple}

We do what we promised to each other, customers, users, and investors.

##### Measure results not hours
{:.no_toc}
We care about what you achieve; the code you shipped, the user you made happy, and the team member you helped. Someone who took the afternoon
off shouldn't feel like they did something wrong. You don't have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules. Do not incite competition by proclaiming how many hours you worked yesterday. If you are working too long hours talk to your manager to discuss solutions.

##### Give agency
{:.no_toc}
We give people agency to focus on what they think is most beneficial. If a meeting doesn't seem interesting and their active participation is not critical to the outcome of the meeting, they can always opt to decline and during a video call they can work on other things if they want. Staying in the call may still make sense even if you are working on other tasks, so other peers can ping you and get fast answers when needed. This is particularly useful in multi-purpose meetings where you may be involved for just a few minutes.

##### Write promises down
{:.no_toc}
Agree in writing on measurable goals. Within the company we use [public OKRs](/company/okrs/) for that.

##### Growth mindset
{:.no_toc}
You don't always get results and this will result in criticism from yourself and/or others. We believe our talents can be developed through hard work, good strategies, and input from others. We try to hire people based on [their trajectory, not their pedigree](https://hbr.org/2016/01/what-having-a-growth-mindset-actually-means).

##### Global optimization
{:.no_toc}
This name comes from the [quick guide to Stripe's culture](https://stripe.com/us/jobs/candidate-info?a=1#culture). Our definition of global optimization is that you do what is best for the organization as a whole. Don't optimize for the goals of your team when it negatively impacts the goals of other teams, our users, and/or the company. Those goals are also your problem and your job. Keep your team as lean as possible, and help other teams achieve their goals.
In the context of [collaboration](#collaboration), this means that if anyone is blocked by you, on a question, your approval, or a merge request review, your top priority is always to unblock them, either directly or through helping them find someone else who can, even if this takes time away from your own or your team's priorities.

##### Tenacity
{:.no_toc}
We refer to this as "persistence of purpose". As talked about in [The Influence Blog](https://www.learntoinfluence.com/developing-tenacity-when-facing-opposition/) tenacity is the ability to display commitment to what you believe in. You keep picking yourself up, dusting yourself off, and quickly get going again having learned a little more.

##### Ownership
{:.no_toc}
We expect team members to complete tasks that they are assigned. Having a task means you are responsible for anticipating and solving problems. As an owner you are responsible for overcoming challenges, not suppliers, or other team members. Take initiative and pro-actively inform stakeholders when there is something you might not be able to solve.

##### Sense of urgency
{:.no_toc}
At an exponentially scaling startup time gained or lost has compounding effects. Try to get the results as fast as possible so the compounding of results can begin and we can focus on the next improvement.

##### Ambitious
{:.no_toc}
While we iterate with small changes, we strive for large, ambitious results.

##### Bias for Action
{:.no_toc}
It's important that we keep our focus on action, and don't fall into the trap of analysis paralysis or sticking to a slow, quiet path without risk. Decisions should be thoughtful, but delivering fast results requires the fearless acceptance of occasionally making mistakes; our bias for action also allows us to course correct quickly.

##### Accepting Uncertainty
{:.no_toc}
The ability to accept that there are things that we don’t know about the work we’re trying to do, and that the best way to drive out that uncertainty is not by layering analysis and conjecture over it, but rather accepting it and moving forward, driving it out as we go along. Wrong solutions can be fixed, but non-existant ones aren’t adjustable at all. [The Clever PM Blog](https://www.cleverpm.com/2018/08/23/accepting-uncertainty-is-the-key-to-agility/)

### Efficiency
{:.gitlab-purple}

We care about working on the right things, not doing more than needed, and not duplicating work. This enables us to achieve more progress, which makes our work more fulfilling.

##### Boring solutions
{:.no_toc}
Use the simplest and most boring solution for a problem, and remember that [“boring” should not be conflated with “bad” or “technical debt.”](http://mcfunley.com/choose-boring-technology) The speed of innovation for our organization and product is constrained by the total complexity we have added so far, so every little reduction in complexity helps. Don’t pick an interesting technology just to make your work more fun; using established, popular tech will ensure a stabler and more familiar experience for you and other contributors.

##### Efficiency for the right group
{:.no_toc}
Optimize solutions globally for the broader GitLab community.  Making a process efficient for one person or a small group may not be the efficient outcome for the whole GitLab community.  As an example, it may be best to choose a process making things slightly less efficient for you while making things massively more efficient for thousands of customers.  For example: choose to replace a renewal process that requires 1,000s of customers to spend 2 hours each with one that only takes 60 seconds even when it may make a monthly report less efficient internally! In a decision, ask yourself "for whom does this need to be most efficient?"  Quite often, the answer may be your users, contributors, customers, or team members that are dependent upon your decision.

##### Be respectful of others' time
{:.no_toc}
Consider the time investment you are asking others to make with meetings and a permission process. Try to avoid meetings, and if one is necessary, try to make attendance optional for as many people as possible. Any meeting should have an agenda linked from the invite, and you should document the outcome. Instead of having people ask permission, trust their judgment and offer a consultation process if they have questions.

##### Spend company money like it's your own
{:.no_toc}
Every dollar we spend will have to be earned back; be as frugal with company money as you are with your own.

##### Frugality
{:.no_toc}
[Amazon states it best](http://www.amazon.jobs/principles) with: "Accomplish more with less. Constraints breed resourcefulness, self-sufficiency and invention. There are no extra points for growing headcount, budget size or fixed expense.".

##### ConvDev
{:.no_toc}
We work according to the principles of [conversational development](http://conversationaldevelopment.com/).

##### Freedom
{:.no_toc}
You should have clear objectives and the freedom to work on them as you see fit.

##### Short verbal answers
{:.no_toc}
Give short answers to verbal questions so the other party has the opportunity to ask more or move on.

##### Keep broadcasts short
{:.no_toc}
And keep one-to-many written communication short, as mentioned in [this HBR study](https://hbr.org/2016/09/bad-writing-is-destroying-your-companys-productivity): "A majority say that what they read is frequently ineffective because it’s too long, poorly organized, unclear, filled with jargon, and imprecise."

##### Managers of one
{:.no_toc}
We want team members to be [a manager of one](https://signalvnoise.com/posts/1430-hire-managers-of-one) who doesn't need daily check-ins to achieve their goals.

##### Responsibility over rigidity
{:.no_toc}
When possible we give people the responsibility to make a decision and hold them accountable for that instead of imposing rules and approval processes.

##### Accept mistakes
{:.no_toc}
Not every problem should lead to a new process to prevent them. Additional processes make all actions more inefficient, a mistake only affects one.

##### Move fast by shipping the minimum viable change
{:.no_toc}
We value constant improvement by iterating quickly, month after month. If a task is too big to deliver in one month, cut scope.

### Diversity
{:.gitlab-purple}

Diversity and inclusion are fundamental to the success of GitLab. We aim to make a significant impact in our efforts to foster an environment where everyone can thrive. We are designing a multidimensional approach to ensure that GitLab is a place where people from every background and circumstance feel like they belong and contribute. We hire globally and encourage hiring in a diverse set of countries. We work to make everyone feel welcome and to increase the participation of underrepresented minorities and nationalities in our community and company. For example our sponsorship of [diversity events](/2016/03/24/sponsorship-update/) and a [double referral bonus](/handbook/incentives/#referral-bonuses).

##### Culture fit is a bad excuse
{:.no_toc}
We don't hire based on culture or select candidates because we'd like to have a drink with them. We hire and reward employees based on our shared values as detailed on this page. We want a **values fit**, not a culture fit.
We want cultural diversity instead of cultural conformity, for example, a [brogrammer](https://en.wikipedia.org/wiki/Brogrammer) atmosphere. Said differently: ["culture add" > "culture fit"](https://twitter.com/Una/status/846808949672366080) or "hire for culture contribution" since our [mission is everyone can contribute](/company/strategy/#mission).

##### Don't bring religion or politics to work
{:.no_toc}
We don't discuss religion or politics because it is easy to alienate people that have a minority opinion. Feel free to mention you attended a ceremony or rally, but don't mention the religion or party.

##### Quirkiness
{:.no_toc}
Unexpected and unconventional things make life more interesting.
Celebrate and encourage quirky gifts, habits, behavior, and points of view. An example
is our [company call](/handbook/communication/#company-call) where we spend most of our time talking about what we did in our private lives, from fire-throwing to
knitting. Open source is a great way to interact with interesting
people. We try to hire people who think work is a great way to express themselves.

##### Building a safe community
{:.no_toc}
Everyone has the right to feel safe when working for GitLab and/or being a part of the GitLab community contributing to our community.  We do not tolerate abuse, [harassment](/handbook/anti-harassment/), exclusion, discrimination or retaliation by/of any community members, including our employees.

##### Unconscious bias
{:.no_toc}
We are responsible not only for what we intend to say but also for the effect it creates.  This includes not only obvious disrespect or lack inclusion but also the everyday things - well-intentioned people nevertheless unintentionally injuring one another due to ignorance.  That means we [all need to get better](/company/culture/inclusion/) at recognizing unconscious or implicit bias in ourselves.

##### Inclusion
{:.no_toc}
Rather than focusing on building diversity as a collection of activities, data, and metrics, we're choosing to [build and institutionalize](http://www.russellreynolds.com/en/Insights/thought-leadership/Documents/Diversity%20and%20Inclusion%20GameChangers%20FINAL.PDF) a culture that is [inclusive](/company/culture/inclusion/) and supports all employees equally to achieve their professional goals. We will refer to this intentional culture curation as inclusion and development (i & d).

##### Inclusive benefits
{:.no_toc}
We list our [Transgender Medical Services](/handbook/benefits/inc-benefits-us/#transgender-medical-services) and [Parental Leave](/handbook/benefits/#parental-leave) publicly so people don't have to ask for them during interviews.

##### Inclusive language
{:.no_toc}
In our [general guidelines](/handbook/general-guidelines/) we list: 'Use inclusive language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys".'

##### Inclusive interviewing
{:.no_toc}
As part of our [interviewing process](/handbook/hiring/interviewing/) we list: "The candidate should be interviewed by at least one female GitLab team member. The GitLab team understands the importance of Inclusive interviewing. We thrive to ensure our hiring team is well versed in every aspect of Diversity, Inclusion and Cultural competence. A positive and comfortable candidate experience is priority."

##### See Something, Say Something
{:.no_toc}
As a global distributed company, we have employees from many different backgrounds and cultures. That means that it is important for each of us to use great judgment in being respectful and inclusive of our teammates. At the same time, we may sometimes make a mistake. It is important that our teammates let us know when we have made a mistake so we can learn from that mistake and gain additional understanding of perspectives different from our own. It is also important that our teammates don't feel excluded or minimized but the words we use or the things we do. Thus, we all need to speak up when we see something that isn't respectful or inclusive. If somebody says something stupid, tell them and help them understand why.

##### Neurodiversity
{:.no_toc}
[Neurodiversity](http://neurocosmopolitanism.com/neurodiversity-some-basic-terms-definitions/) is a type of diversity that includes Autism, ADHD, and other styles of neurodivergent functioning. While they often bring [unique skills and abilities](https://adhdatwork.add.org/potential-benefits-of-having-an-adhd-employee/), which can be harnessed for [competitive advantage](https://hbr.org/2017/05/neurodiversity-as-a-competitive-advantage), neurodivergent individuals are often discriminated against, and sometimes have trouble making it through traditional hiring processes. These individuals should be able to contribute as GitLabbers. The handbook, values, strategy, and interviewing process should never discriminate against the neurodivergent.

##### Family and friends first, work second
{:.no_toc}
Long lasting relations [are the rocks of life](https://www.youtube.com/watch?v=6_N_uvq41Pg) and come before work. As someone said in our #thanks channel, after helping a family member for 5 days after a hurricane: "THANK YOU to GitLab for providing a culture where "family first" is truly meant".

### Iteration
{:.gitlab-purple}

We do the [smallest thing possible and get it out as quickly as possible](/2017/01/04/behind-the-scenes-how-we-built-review-apps/). If you make suggestions that can be excluded from the first iteration turn them into a separate issue that you link. Don't write a large plan, only write the first step. Trust that you'll know better how to proceed after something is released. You're doing it right if you're slightly embarrassed by the minimal feature set shipped in the first iteration. This value is the one people underestimate when they join GitLab, the impact both on your work process and on how much you achieve is greater than anticipated. In the beginning it hurts to make decisions fast and to see that things are changed with less consultation. But frequently the simplest version turns out to be the best one.

People that join GitLab all say they already practice this iteration. But this is the value that they have the hardest time adopting. People are trained that if you don't deliver a perfect or polished thing you get dinged for it. If you do just one piece of something you have to come back to it. Doing the whole thing seems more efficient, even though it isn't. If the complete picture is not clear your work might not be perceived as you want it to be perceived. It seems better to make a comprehensive product. They see other people in the GitLab organization being really effective with iteration but don't know how to make the transition, and it's hard to shake the fear that constant iteration can lead to shipping lower-quality work or a worse product.

The way to resolve this is to write down only what you can do with the time you have for this project right now. That might be 5 minutes or 2 hours. Think of what you can complete in that time that would improve the current situation. Iteration can be uncomfortable, even painful. If you're doing iteration correctly, it should be.

However, if we take smaller steps and ship smaller simpler features, we get feedback sooner. Instead of spending time working on the wrong feature or going in the wrong direction, we can ship the smallest product, receive fast feedback, and course correct. People might ask why something was not perfect. In that case, mention that it was an iteration, you spent only "x" amount of time on it, and that the next iteration will contain "y" and be ready on "z.""

##### Don't wait
{:.no_toc}
Don’t wait. When you have something of value like a potential blog post or a small fix implement it straight away. Right now everything is fresh in your head and you have the motivation, inspiration is perishable. Don’t wait until you have a better version. Don’t wait until you record a better video. Don’t wait for an event (like Contribute). Inventory that isn’t released is a liability since it has to be managed, gets outdated, and you miss out on the feedback you get when you would have implemented it straight away.

##### Reduce cycle time
{:.no_toc}
Short iterations reduce [our cycle time](/product/cycle-analytics/).

##### Work as part of the community
{:.no_toc}
Small iterations make it easier to work with the wider community. Their work looks more like our work and our work is quicker to give feedback too.

##### Minimum Viable Change (MVC)
{:.no_toc}
Always look to make the quickest change possible to improve the outcome. If you think it is better than what is there now do it. No need to wait for something polished. [More information is in the product handbook](/handbook/product/#the-minimally-viable-change) but it applies to everything we do in all functions.

##### Make a proposal
{:.no_toc}
If you need to decide something as a team make a concrete proposal instead of calling a meeting to get everyone's input. Having a proposal will be a much more effective use of everyone's time. Every meeting should be a review of a proposal. We should be [brainwriting on our own instead of brainstorming out loud](https://www.fastcompany.com/3033567/brainstorming-doesnt-work-try-this-technique-instead). State the underlying problem so that people have enough context to propose reasonable alternatives. The people that receive the proposal should not feel left out and the person making it should not feel bad if a completely different proposal is implemented. Don't let your ego to be involved early or to see your solution implemented stand in the way of getting to the best outcome. If you don't have a proposal don't let that stop you from highlighting a problem, please state that you couldn't think of a good solution and list any solutions you considered.

##### Everything is in draft
{:.no_toc}
At GitLab we rarely put draft on any content or proposals. Everything is always in draft and subject to change.

##### Under construction
{:.no_toc}
As we get more users they will ask for stability, especially in our UX. We should always optimize for the long term. This means that users will be inconvenienced in the short term, but current and future users will enjoy a better product in the end.

##### Low level of shame
{:.no_toc}
When we talked to Nat Friedman he said: "A low level of shame is intrinsic to your culture.". This captures the pain we feel by shipping something that isn't where we want it to be yet.

##### Focus on Improvement
{:.no_toc}
We believe great companies sound negative because they focus on what they can improve, not on what is working.
Our first question in every conversation with someone outside the company should be: what do you think we can improve?
This doesn't mean we don't recognize our successes, for example see also our [Say Thanks](#say-thanks) value.
We are positive about the future of the company, we are present day pessimists and long term optimists.

##### Do things that don't scale
{:.no_toc}
First optimize for speed and results, when it is a success figure out how to scale it. Great example are in [this article by Paul Graham](http://paulgraham.com/ds.html).

##### Make two way door decisions
{:.no_toc}
Most decisions are easy to reverse, have the [directly responsible individual](/handbook/people-operations/directly-responsible-individuals/) make them without approval. As [Jeff Bezos describes](http://minimumviablestrategy.com/lessons/leadership/one-way-and-two-way-door-decisions/) only when you can't reverse them, there should be a more thorough discussion.

##### Changing proposals isn't iteration
{:.no_toc}
Changing a proposal isn't iterating. Only when the change is rolled out to users can you learn from feedback. When you're changing a proposal based on different opinions you're frequently wasting time, it would be better to roll out a small change quickly and get real world feedback.

A few challenges have arisen with how we approach iteration. The best example may be the proposal of a 2-month release cycle. The argument was that a longer release cycle would buy us time for bug fixes and feature development, but we don’t believe that is the case. As detailed above, we aim to make the absolute smallest thing possible and that doing otherwise will only slow us down. 

That said, we would love to work on a 2-week release cycle, but that should be another conversation.

##### Make small merge requests
{:.no_toc}

When you are submitting a merge request for a code change, or a process change in
the handbook, keep it is small as possible. If you are adding a new page to the
handbook, create the new page with a small amount of initial content, get it merged 
quickly, and then add additional sections iteratively with subsequent merge requests. 
If you are asked to review a merge request that is too big, consider kindly asking 
the author to change it to a smaller size before reviewing.

### Transparency
{:.gitlab-purple}

Be open about as many things as possible. By making information
public we can reduce the threshold to contribution and make collaboration easier.

An example is the [public repository of this website](https://gitlab.com/gitlab-com/www-gitlab-com/)
that also contains this [company handbook](/handbook/). Everything we do is public by default, for example, the [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/issues) and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/issues) issue trackers, but also [marketing](https://gitlab.com/groups/gitlab-com/marketing/-/issues) and [infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues). Transparency creates awareness for GitLab, allows us to recruit people that care about our values, it gets us more and faster feedback from people outside the company, and makes it easier to collaborate with them. It is also about sharing great software, documentation, examples, lessons,
and processes with the world in the spirit of open source, which we believe creates more value than it captures.

There are exceptions, though. Material that is [not public by default is documented in the general guidelines](/handbook/general-guidelines/#not-public). We are above average at keeping things confidential that need to be.  On a personal level, you should tell it like it is instead of putting up a poker face. Don't be afraid to admit you made a mistake or were wrong. When something went wrong it is a great opportunity to say "What’s the [kaizen](https://en.wikipedia.org/wiki/Kaizen) moment here?" and find a better way without hurt feelings.

Even as we [move towards becoming a public company](/company/strategy) and beyond, we know that our value of transparency will be key to our success. Often company values get diluted as they grow, most likely because they do not write anything down. But we will make sure our values scale with the company. When we go public, we can declare everyone in the company as an insider, which will allow us to remain transparent internally about our numbers, etc. Everything else that can be transparent will continue to be so.

##### Public by default
{:.no_toc}
Everything at GitLab is public by default. If something is not public there should be a reference in the handbook that states a confidential decision was taken with a link to our [Not Public guidelines](/handbook/general-guidelines/#not-public) unless legal feels it carries undue risk.

##### Directness
{:.no_toc}
Is about being transparent with each other. We try to channel our inner Ben Horowitz by being [both straightforward
and kind, an uncommon cocktail of no-bullshit and no-asshole](https://medium.com/@producthunt/ben-horowitz-s-best-startup-advice-7e8c09c8de1b). Feedback is always about your work and not your person. That doesn't mean it will be easy to give nor receive it.

##### Surface issues constructively
{:.no_toc}
Be transparent to the right people (up) at the right time (when still actionable)

##### Anyone and anything can be questioned
{:.no_toc}
Any past decisions and guidelines are open to questioning as long as you act in accordance with them until they are changed.

##### Disagree, commit, and disagree
{:.no_toc}
Everything can be questioned but as long as a decision is in place we expect people to commit to executing it, which is [a common principle](http://ryanestis.com/leadership/disagree-and-commit-to-get-things-done/). Every decision can be changed, our [best decision was ones that changed an earlier one](https://youtu.be/4BIsON95fl8?t=2034). When you want to reopen the conversation on something show that your argument is informed by previous conversations. You have to achieve results on every decision while it stands, even when you are trying to have it changed. You should communicate with the person who can change the decision instead of someone who can't.

##### Transparency is only a value if you do it when it is hard
{:.no_toc}
For example, many companies in California do not give you the real reason why they declined your application because it increases the chance of legal action. We want to only reject people for the right reasons and we want to give them the opportunity to grow by getting this feedback. Therefore, we'll accept the increased risk of holding ourselves to a high standard of making decisions and do the right thing by telling them what we thought. Other examples are being transparent about [security incidents](http://disq.us/p/1r9gceh), and participating in and contributing to Live Broadcasts.

##### Single Source of Truth
{:.no_toc}
By having most company communications and work artifacts be internet-public, we have one single source of truth for all GitLab team members, users, customers, and other community members. We don‘t need separate artifacts with different permissions for different people.

##### Find the Limit
{:.no_toc}
We accept that we occasionally make mistakes in the direction of transparency. In other words, we accept it if we're sometimes publicizing information that should have remained confidential in retrospect. Most companies become non-transparent over time because they don't accept any mistakes. Making mistakes and reflecting on them means we know where the limit of transparency is.

##### Say why, not just what
{:.no_toc}
Transparent changes have the reasons for the change laid out clearly along with the change itself. This leads to fewer questions later on because people already have some understanding. A change with no public explanation can lead to a lot of extra rounds of questioning which is less efficient.  Avoid using terms such as "industry standard" or "best practices" as they are vague, opaque, and don't provide enough context as a reason for a change. 

##### Reproducibility
{:.no_toc}
Enable everybody involved to come to the same conclusion as you. This does not only involve [reasoning](#say-why-not-just-what) but also for example providing raw data and not just plots, scripts to automate tasks and not just the work they have done, and documenting steps while analyzing a problem. Do your best to make the line of thinking transparent to others even [if they may disagree](/handbook/leadership/#making-decisions).

##### Accountability
{:.no_toc}
Increases accountability when making decisions and difficult choices

## Five dysfunctions

Our values help us to prevent the [five dysfunctions](https://en.wikipedia.org/wiki/The_Five_Dysfunctions_of_a_Team#Summary).

1. **Absence of trust** Unwilling to be vulnerable within the group => _prevented by collaboration, specifically kindness_
1. **Fear of conflict** Seeking artificial harmony over constructive passionate debate => _prevented by transparency, specifically directness_
1. **Lack of commitment** Feigning buy-in for group decisions creates ambiguity throughout the organization => _prevented by transparency, specifically directness_
1. **Avoidance of accountability** Ducking the responsibility to call peers on counterproductive behavior which sets low standards => _prevented by results, iteration, and transparency_
1. **Inattention to results** Focusing on personal success, status and ego before team success => _prevented by results_

Some dysfunctions are not addressed directly by our values, for example trust is not one of our values.
Similar to happiness, trust is something that is an outcome, not something you can strive for directly.
We hope that the way we work and our values will instill trust, instead of mandating it from people; trust is earned, not given.

## Why have values

Our values should give guidelines on how to behave and must be actionable.
They help us describe the type of behavior that we expect from people we hire.
They help us to know how to behave in the organization and what to expect from others.
Values are a framework for distributed decision making, they allow you to determine what to do without asking your manager.

## Hierarchy

Occasionally, values can contradict each other. For instance, transparency would dictate we publish all security vulnerabilities the moment they are found, but this would jeopardize our users. It's useful to keep in mind this hierarchy to resolve confusion about what to do in a specific circumstance, while remaining consistent with our core values.

<table style="text-align:center; border-collapse:separate;">
  <tr>
    <td colspan="2" style="border:0;">&nbsp;</td>
    <td colspan="2" style="border:1px solid;"><strong>R</strong>esults</td>
    <td colspan="2" style="border:0;">&nbsp;</td>
    <td style="border:0; text-align:left;">It is most important to focus on results.</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
    <td colspan="2" style="border:1px solid"><strong>I</strong>teration</td>
    <td colspan="2" style="border:1px solid"><strong>T</strong>ransparency</td>
    <td style="border:0;">&nbsp;</td>
    <td style="border:0; text-align:left;">We trust these values will lead to results.</td>
  </tr>
  <tr>
    <td colspan="2" style="border:1px solid;"><strong>C</strong>ollaboration</td>
    <td colspan="2" style="border:1px solid;"><strong>D</strong>iversity</td>
    <td colspan="2" style="border:1px solid;"><strong>E</strong>fficiency</td>
    <td style="border:0; text-align:left;">Distinguish us from other companies.</td>
  </tr>
</table>

## Updating our values

Our values are updated continually when needed. Everyone is welcome to make a suggestion to improve them. To update: make a merge request and assign it to the CEO. If you're a [team member](/company/team/) or in the [core team](/community/core-team/) please post a link to the MR in the #values channel. If you're not part of those groups please send a direct Twitter message to [@sytses](https://twitter.com/sytses).

## How do we reinforce our values

Whatever behavior you reward will become your values. We reinforce our values through the following actions:

1. By what we do, especially what the [leadership](/handbook/leadership/) does.
1. By what we select for during [hiring](/handbook/hiring/).
1. By what we emphasize during [on-boarding](/handbook/general-onboarding/).
1. By what behavior we give each-other [360 feedback](/handbook/people-operations/360-feedback/) on.
1. By what behavior we [compliment](/handbook/communication/#say-thanks).
1. By what criteria we use for [discretionary bonuses](/handbook/incentives/#discretionary-bonuses).
1. By what criteria we use for our [annual compensation review](/handbook/people-operations/global-compensation/#annual-compensation-review)
1. By what criteria we use for [promotions](/handbook/people-operations/promotions-transfers/).
1. By what criteria we use [manage underperformance](/handbook/underperformance/).
1. By what we do when we [let people go](/handbook/offboarding/).

In negative feedback we should be specific about what the problem is, saying someone is "[not living the values](https://hbr.org/2017/05/how-corporate-values-get-hijacked-and-misused)" isn't helpful.

## Permission to play

From our values we excluded some behaviors that are obvious, we call them our permission to play behavior:

- Be truthful and honest.
- Be dependable, reliable, fair, and respectful.
- Be committed, creative, inspiring, and passionate.
- Be deserving of the trust of our users and customers.
- Act in the best interest of the company, our team members, our customers, users, and investors.
- Act in accordance with the law.

## Question from new team members

During every [GitLab 101 session with new hires](/company/culture/gitlab-101/) we discuss our values. We document the questions and answers to [Frequently Asked Questions about the GitLab Culture](/company/culture/gitlab-101/#frequently-asked-questions-about-the-gitlab-culture).

## Mission

Our [mission](/company/strategy/#mission) guides our path, during this path live our values.
