---
layout: markdown_page
title: "The GitLab Legal Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}
 
# The GitLab Legal Team as your Trusted Partner and Advisor

## Contacting the Legal Team with General Legal Questions

You can reach out to the Legal Team on the *[`#legal`](https://gitlab.slack.com/archives/legal)* Slack chat channel.  The legal Slack chat channel is reserved for everyday legal questions. If you are making a request that requires some sort of deliverable, please do not use the legal Slack chat channel. Slack is reserved for immediate, informal communications. Also, please do not share confidential information on Slack that is not meant for the entire company to see, and do not use it to seek legal advice. You can email the legal team at *[legal@gitlab.com](mailto:legal@gitlab.com)*. And of course you can always send a direct Slack message or email to anyone on the Legal Team, if preferred.

## Requesting Legal Services
 
If you need to request legal services, there are three main processes, depending on your legal need. 

### 1. Vendor Contracts

If you are seeking review of a vendor contract, your request must go through GitLab's Vendor Management process to make sure it has all of the necessary eyes on it, and that the request receives all of the required approvals.  This process involves Finance (for budgetary purposes), Security (when personal data is involved to ensure the vendor's systems and procedures meet our minimum standard), Business Operations, Legal (for contract review), and Paul (for final approval and signing).  For instructions on how to submit a new vendor contract for review, please see the [Vendor and Contract Approval Workflow](https://about.gitlab.com/handbook/finance/procure-to-pay/#vendor-and-contract-approval-workflow).

### 2. Sales Contracts Using GitLab's Paper

If a customer is requesting an editable version of any of our standard template agreements, the entire process will take place within Salesforce using Conga Contracts.  You can find the step by step process on the [Sales Order Processing](https://about.gitlab.com/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable) page.

### 3. Requests for Insurance Certificate

If you need an insurance certificate (other than for worker's compensation) you can send an email request to *[email](mailto:abdcertdept@theabdteam.com).*    You will need to include contact information for the customer seeking to be added to the certificate and any other specific requirements relating to the coverage.  Please copy legal on the request. If you require an insurance certificate for worker's compensation email: payroll@gitlab.com with the same information (and copy legal).

### 4. All Other Legal Requests (_including Sales Contracts but using the Customer's Paper_)

For all other requests such as customer contracts using the customer's paper, NDA's, legal requests that have a deliverable and are not appropriate for Slack, assistance with customer questionnaires or compliance questions, please use the private Legal Issue Tracker by submitting your request to the Legal *[email](mailto:legal@gitlab.com)* address. This will send your request to the Private Legal Issue Tracker using GitLab's Service Desk functionality. Through the legal issue tracker, you will not be able to access the issue itself, but instead will be updated regarding the status of your request through email. Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond. Please note that **ONLY** the Executive Team and Sales Directors have access to the [Legal Issue Tracker](https://gitlab.com/gitlab-legal/legal-issue-tracker) in order to maintain the confidentiality and privilege of any issues that may be discussed within the Legal Issue Tracker. For more information on Attorney-Client Privilege, see the [General Topics and FAQs](handbook/legal/#general-topics-and-faqs) below. 
 
# Contract Templates
 
- [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)
- [Data Processing Addendum](https://docs.google.com/document/d/e/2PACX-1vQVuCNstb-xPYPW-xWdqyhCga2tW1sHMFWGAHsxfde0ylrub93iExo9PVQWPBn-wzNBlMiJRNx-wigD/pub)
- [SaaS Addendum](https://drive.google.com/file/d/1NwaYid6qIJk9YscaRoY-uY5bEGsN1uu2/view?usp=sharing)

# General Topics and FAQs

## 1. The Attorney-Client Relationship in the United States

This discussion is limited to U.S. practices because currently our team members only communicate with U.S. practicing attorneys to which.  As we continue to grow globally we will update this and expand how privilege applies in other jurisdictions.

### What is the Attorney-Client Privilege?

Attorney-Client Privilege is a law that has been adopted in each of the states of the U.S in some form.  Generally, the law protects communications between clients and their attorneys for the purpose of seeking legal guidance and advice.  The information is not protected if it is available from another source.  Therefore, information cannot be placed under the protections of Attorney-Client privilege simply by telling your attorney or copying your attorney on a communication.  In addition, the underlying facts are also not protected, only the opinions and analysis of the facts, and discussions thereof, with the attorney.  The privilege belongs to the client, and therefore, can only be waived by the client.

### What is Work Product?

Work Product is a U.S. doctrine in which an attorney’s notes, observations, thoughts, and research prepared by, or at the direction of an attorney, in anticipation of litigation, are protected from being discoverable during the litigation process.

### What is the purpose of these Privileges?

The purpose of the Attorney-Client and Work Product privileges is to allow clients to speak freely with their attorneys and encourage full disclosure so they can receive accurate and competent legal advice without the fear of having their attorney compelled to testify against them and disclose the information shared by the client.

### Who do these Privileges Apply to at GitLab?

There is not one uniform answer that covers all jurisdictions in the U.S. 

A minority number of states apply the **Corporate Group Test**. This test is quite restrictive and only allows for the protection of corporate communications to the corporation's controlling executives and managers.

A more commonly used test is the **Subject Matter Test**.  Instead of looking at the roles of the employees involved, this test looks at the subject matter of the employees’ communications. The test will look to see if the employee was instructed to discuss the subject matter with the attorney should be protected and if the subject matter of that communication relates to the performance by the employee of the duties of his or her employment.

A slightly modified version of the **Subject Matter Test** called the **Upjohn Test** is also widely used. Under the **Upjohn Test** the privilege is applied only if the following criteria are satisfied:

* The employee must have made the communication to counsel for the purpose of seeking legal advice regarding the corporation.
* The substance of the communication must involve matters that fall within the scope of the employee's job duties.
* The employee must be aware that the statements are being provided for the purpose of obtaining legal advice for the corporation.
* The communications also must be confidential when made and must be kept confidential by the company.

The Supreme Court case which established the  **Upjohn Test** is also important because it resulted in the **_Upjohn Warning_** which is a procedure in which a company’s attorney explains that he or she does not represent the employee individually, but instead represents the interests of the company.  This is important to note because a company can waive its privilege at any time, meaning the company could choose to disclose information the attorney received from a covered employee in confidence for use as evidence in a legal proceeding in order to protect the company from liability.
 
 The **Subject Matter Test** and **Upjohn Test** are the most commonly used tests. More information about the tests can be found [HERE](https://drive.google.com/open?id=1Rzm_oFo-nGx8RWZgSaZBOCPBL1kCrce3)
 
### How do you Protect the Privileges that Apply to You When Seeking Legal Advice?
 
 * Direct the communication to a practicing licensed attorney.  Privilege does not apply to other non-attorney members of the legal team.
 * It is best practice to have privileged conversations with the attorney via Zoom.
 * If other individuals will need to participate in the discussion, consult with the attorney, and only include the minimum necessary individuals in the conversation, in other words, keep the circle of trust small.
 * If it is necessary to communicate by email, in the “Subject” line, and at the top of the body of the communication, include the phrase **“AC PRIV”**.
 * Do not overuse the claim of privilege. Limit its use to when actually seeking legal guidance and advice and not on any and every correspondence with the attorney.
 * You must keep the information discussed confidential, and not share it with anyone outside the circle of trust without first consulting with the attorney.
 * Do not forward protected emails to anyone outside the circle of trust.
 * Do not copy anyone outside the the circle of trust on emails with the attorney.
 * As with anything else, if you ever have any questions or concerns, do not hesitate to reach out to the Legal team.

## 2. Litigation Holds

### What is a Litigation Hold?

A litigation hold is the process a company uses to preserve all forms of relevent evidence, whether it be emails, instant messages, physical documents, handwritten or typed notes, voicemails, raw data, backup tapes, and any other type of information that could be relevant to pending or imminent litigation or when litigation is reasonably anticipated.  Litigation holds are imperative in preventing spoliation (destruction, deletion, or alteration) of evidence which can have a severely negative impact on the company's case, including leading to sanctions.

Once the company becomes aware of potential litigation, the company's attorney will provide notice to the impacted employees, instructing them not to delete or destroy any information relating to the subject matter of the litigtation.  The litigation hold applies to paper and electronic documents. During a litigation hold, all retention policies must be overridden.
 
# Important Pages Related to Legal
 
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
* [General Guidelines](/handbook/general-guidelines/)
* [Terms](/terms/)
* [Privacy Policy](/privacy/)
* [Trademark](/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines)
* [Signing Legal Documents](/handbook/signing-legal-documents/)
* [Authorization Matrix](/handbook/finance/authorization-matrix/)

## Internal Legal Team Processes
 
[Generating Contracts with Conga Contracts](/handbook/legal/customer-negotiations/index.html)

