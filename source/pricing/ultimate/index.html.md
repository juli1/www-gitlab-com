---
layout: markdown_page
title: "Why Ultimate?"
---

## GitLab Ultimate

| Ultimate supports enterprise IT transformations to high-velocity delivery without sacrificing security, compliance or enterprise governance. Ultimate helps enterprises improve: <br> <br> **- Portfolio Management** <br> **- Application Security**  <br> **- Compliance** <br> **- Operations** <br><br>  | [![GitLab Roadmap](/images/solutions/scaled-agile/gitlab_roadmap.png)](https://about.gitlab.com/images/solutions/scaled-agile/gitlab_roadmap.png) |

### Security

| Cybersecurity is a critical concern of every business leader.  Your applications MUST be secure. GitLab Ultimate weaves security into the pipeline to provide early and actionable feedback to the development team.  | [![Security Dashboards](/images/feature_page/screenshots/61-security-dashboard.png)](https://about.gitlab.com/images/feature_page/screenshots/61-security-dashboard.png) |


### Portfolio Management

| Establish end to end visibility from Business Idea to delivering value. GitLab Ultimate, enables portfolio planning, tracking, and execution in one tool, that unifies the team to focus on delivering business value. | [![Epics](/images/feature_page/screenshots/51-epics.png)](https://about.gitlab.com/images/feature_page/screenshots/51-epics.png) |

### Operations

| Maintain an end to end picture of how your applications are deployed and delivering business value.  | [![Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png)](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png) |



**Note:** Ultimate also includes 4 business hour support and a Technical Account Manager.

### Specific Ultimate features

| Security    | Value |
| --------- | ------------ |
| Static Application Security Testing | Evaluates the static code, checking for potential security issues.   |
| Dynamic Application Security Testing | Analyzes the review application to identify potential security issues.  |
| Dependency Scanning |  Evaluates the third-party dependencies to identify potential security issues.   |
| Container Scanning |  Analyzes Docker images and checks for potential security issues.  |
| Security Dashboard | Visualize the latest security status for each project and across projects. |
| [*Security Metrics and Trends* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/6954)| *Metrics and historical data about how many vulnerabilities have been spotted, addressed, solved, and how much time was spent for the complete cycle.* |

| Portfolio Management     | Value |
| --------- | ------------ |
| Epics |  Organize, plan, and prioritize business ideas and initiatives. |
| Roadmaps | Visualize the flow of business initiatives across time in order to plan when future features will ship.   |
| [*VSM Workflow Analytics*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/7269) | *Visualize the end to end value stream to identify and resolve bottlenecks.* |
| [*Risk Management* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3978) | *Manage risk of epics not being completed on time.* |
| [*What-If Scenario Planning*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3979) | *Visualize potential impact in the overall portfolio if you were to make a change.* |
| [*Roadmap Capacity Planning*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/6777) | *Visualize if future work is feasible from an effort perspective.* |

| Operations    | Value |
| --------- | ------------ |
| Kubernetes Cluster Health Monitoring | Track cluster CPU and Memory utilization. Keeping an eye on cluster resources can be important, if the cluster runs out of memory pods may be shutdown or fail to start.   |
| Kubernetes Cluster Logs | View the logs of running pods in connected Kubernetes clusters so that developers can avoid having to manage console tools or jump to a different interface. |
| App Perf. Alerts | Respond to changes to your application performance with alerts for custom metrics. |
| [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/index.html#doc-nav) | a holistic view of the overall health of your company's operations.  |
| [*Anomaly Alerts* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3610) | *alerting based on deviation from the weekly mean.* |

### Compliance

| Compliance    | Value |
| --------- | ------------ |
| License Management |  Identify the presence of new software licenses included in your project. Approve or deny the inclusion of a specific license. |
| [*CD with SOC 2 Compliance*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/4120) | *Support SOC 2 compliance* |

### Other

| Other    | Value |
| --------- | ------------ |
| Guest Users|  Guest users don’t count towards the license count.  |


<center><a href="/sales" class="btn cta-btn orange margin-top20">Contact sales and learn more about GitLab Ultimate</a></center>
