---
layout: markdown_page
title: "Category Vision - GitLab Pages"
---

- TOC
{:toc}

## GitLab Pages

GitLab Pages allows you to create a statically generated website from your project that
is automatically built using GitLab CI and hosted on our infrastructure.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=pages&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Please reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) /
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask
questions about what's coming.

### Overall Prioritization

Pages is not strategically our most important Release feature, but it's a popular
feature and one that people really enjoy engaging with as part of the GitLab
experience; it's truly one of our most "personal" features in the Release stage.
We do not plan to provide a market-leading solution for static web page hosting,
but we do want to offer one that is capable for most basic needs, in particular
for hosting static content and documentation that is a part of your software
release.

## What's Next & Why

Automatic certificate renewal ([gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996))
is our most popular issue and one that can be quite irritating to manually
manage. We're excited to address this next in order to make using Pages
easier and require less ongoing maintenance.

## Competitive Landscape

Automatic certificate renewal ([gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996))
is highly popular in almost every category here, and would make adoption
of GitLab Pages much more attractive.

## Top Customer Success/Sales Issue(s)

The most popular customer issue is [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996),
which makes setting up and renewing certificates automatic for your Pages site.
This is an ongoing hassle once you set up your Pages site since renewals in
particular are frequent and manual. Solving this problem will make Pages
even more automatic and easy to use.

## Top Customer Issue(s)

Aligned with what CS is seeing, [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate
renewal) is our top customer issue.

## Top Internal Customer Issue(s)

Similar to many other categories in the vision here at the moment, internal
customers have frequently raised [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate
renewal) as the biggest irritation in working with pages.

Our own docs team is considering moving to a different hosting provider; details
on reasons why can be found at [gitlab-docs#313](https://gitlab.com/gitlab-com/gitlab-docs/issues/313).
The main difficulty with using Pages at [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) 
/ [GitLab documentation](https://gitlab.com/gitlab-com/gitlab-docs) scale is:

- Caching artifacts between pages runs
  - We clear and recreate everything every time, but we could mark certain directories (mark certain folders as "do not rebuild", potentially detect what's different via only/except data), in which case these would just be persisted from the previous run. See comment [gitlab-ce#29496](https://gitlab.com/gitlab-org/gitlab-ce/issues/29498#note_149246568) for how this might look.
  - The alternative is having your own server, where you can obviously just leave files there.

### Other, less urgent items

- Managing binary files inside of the repo that results in slow interactions and big initial download
  - Maybe use LFS? Something else?
- Automatic duplicate checking or orphaned images checking
  - May be quite complicated to detect references
- Redirects are a pain to manage, we only support meta refresh natively. Redirects could be handled in the Netlify way (`_redirects` file): https://www.netlify.com/docs/redirects/
- Review apps are working well right now.
- Neither Middleman nor Nanoc support incremental builds yet, but teams are working on that. Workspaces ([gitlab-ce#47062](https://gitlab.com/gitlab-org/gitlab-ce/issues/47062)) would be valuable for a guaranteed workspace cache between steps/runs.
- Inline comments/issue reporting could also be interesting: [gitlab-ce#53390](https://gitlab.com/gitlab-org/gitlab-ce/issues/53390)

## Top Vision Item(s)

From a vision standpoint, adding Review Apps for Pages ([gitlab-ce#26621](https://gitlab.com/gitlab-org/gitlab-ce/issues/26621))
is interesting because it allows for more sophisticated development flows
involving testing, where at the moment the only environment that GitLab
understands is production. This would level up our ability for Pages to
be a more mission-critical part of projects and groups.
