---
layout: markdown_page
title: "Category Vision - Linux Package Registry"
---

- TOC
{:toc}

## Linux Package Registry

Linux distros depend on linux package regisitries for distribution of installable software. By supporting Debian and RPM we will cater to a big chunk of the market and allow [Systems Administrator](https://design.gitlab.com/research/personas#persona-sidney) tasks to be internalized.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Linux%20Package%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

## What's Next & Why

TODO

## Competitive Landscape

TODO

## Top Customer Success/Sales Issue(s)

TODO

## Top Customer Issue(s)

TODO

## Top Internal Customer Issue(s)

TODO

## Top Vision Item(s)

TODO
