---
layout: markdown_page
title: "Category Vision - Transactions"
---

- TOC
{:toc}

## 🎁 Transactions

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

* [Overall Vision](/direction/fulfillment)
* [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=transactions) 
* [Maturity: Minimal](/product/categories/maturity)
* [Documentation](https://docs.gitlab.com/ee/getting-started/subscription.html)
* [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=transactions)

The Transactions category covers the [Customers application](https://gitlab.com/gitlab-org/customers-gitlab-com/), more commonly referred to as the Customer Portal or the Subscription Portal. This was created to serve as a billing engine for both self-managed and GitLab.com users. This is a separate application to GitLab, and requires a separate login to GitLab.com. Typically, users use the Transactions portal to purchase and manage their GitLab Plan subscriptions and GitLab add-ons, although some customers may handle their transactions through the GitLab Sales team or a Reseller. All of these user journies, both users of GitLab and GitLab team members, are the responsibility of Fulfillment.

Please reach out to  Luca Williams, Product Manager for Fulfillment ([E-Mail](luca@gitlab.com)/
[Twitter](https://twitter.com/tipyn2903)) if you'd like to provide feedback or ask
questions about what's coming.

## 🔭 Vision

Engaging in business transactions with GitLab should be minimally invasive and when noticed, should be a positive, empowering and user-friendly experience. Our goal is to fulfill the needs of users all around the world in a manner that does not exclude them through a lack of flexibility in payment methods, currencies or billing models.

As part of the overall Transactions vision, users should:

* Be enabled and empowered to [serve themselves](https://gitlab.com/groups/gitlab-org/-/epics/923) where possible when needing to manage their payments and account details without having to wait for a GitLab team member to provide assistance. 
* Be able to tweak their usage needs and requirements through flexibility in purchasable add-ons to their GitLab Plans.
* Not feel alienated from the GitLab product by being transported to a visibly separated portal application. Users should always feel [at home](https://gitlab.com/groups/gitlab-org/-/epics/742) within the GitLab design experience ecosystem.
* Feel at ease knowing that their information is [secure](https://gitlab.com/groups/gitlab-org/-/epics/939) within the Transactions portal and the portal meets the same security requirements as GitLab.
* Be able to rely upon the portal availability. The Transactions portal should be [stable and high-performing](https://gitlab.com/groups/gitlab-org/-/epics/1177), reducing any possible outages for both GitLab Team members and GitLab users.
* Be able to pay in their [own currency](https://gitlab.com/groups/gitlab-org/-/epics/931).
* Be able to pay [using a payment method](https://gitlab.com/groups/gitlab-org/-/epics/929) that makes sense for them.
* Be entitled to a [fair billing model](https://gitlab.com/groups/gitlab-org/-/epics/1178) and only pay for what they use.
* Be able to purchase GitLab from more applicable [external marketplaces](https://gitlab.com/groups/gitlab-org/-/epics/520) for their business.
* Be able to view [insights](https://gitlab.com/groups/gitlab-org/-/epics/938) into their billing history and user activity.
* Be able to stay informed by visiting their Transactions [notifications center](https://gitlab.com/groups/gitlab-org/-/epics/934) and never miss an important message due to email typos or spam filtering.

We should also aim to make the Transactions portal [provider independent](https://gitlab.com/groups/gitlab-org/-/epics/922), eliminating risk of down time due to subscription provider issues, and increasing GitLab's capability to always be able to work with a provider that aligns with our present needs and requirements.

Design and user experience is an overall priority for Fulfillment. Please visit this [meta epic](https://gitlab.com/groups/gitlab-org/-/epics/1176) to see ongoing design and UX improvements to the Fulfillment group's categories.

## 🎭 Target audience and experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

### Sidney (Systems Administrator) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)

There may be organisations that have multiple instances, so one billing administrator and multiple technical adminsrators.
For small businesses, it’s more likely that they’re the same person. In general, the larger the organization, the more likely it is that personas will be separate people.

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

### Evan (Billing Administrator) - [Persona Description TBD]
Evan generally does not use GitLab as part of their day-to-day responsibilities and may not have a GitLab account, since the organization may not want to use an additional seat. Their job is to make sure the different teams have access to the tools they need.

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

### Skye (GitLab Billing/Sales Administrator) - [Persona Description TBD]

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

### Kennedy (Sales Representative) - [Persona Description TBD]

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

For other details on the current journey for these personas, see a recent [journey map](https://docs.google.com/document/d/13_cQ9L8mWudy3qQRJYiLEYiqiEmgLqHw1f1ZGTPgCPM/edit#heading=h.e6phc2cw5ql). For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

## 🚀 What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

Our biggest priorities focus around the goal of providing self-service options for users, reducing the need for manual intervention of GitLab team members and user support requests. 

We will concentrate on understanding what a great user experience for users and GitLab team members may look like, and perform deep user research and planning prior to engineering implementation. Please see [🏪 Build a fully self-serviced transactions portal](https://gitlab.com/groups/gitlab-org/-/epics/923) for more information.

In addition, we will be prioritising further iterations around GitLab usage-based add-ons, such as add-on [CI Runner minutes](https://gitlab.com/groups/gitlab-org/-/epics/851) and [add-on storage](https://gitlab.com/groups/gitlab-org/-/epics/586). Allowing users to purchase these provides a more fine-tuned experience for individual users and organisations based on their usage and reduces the possibility of any interruptions in service due to GitLab Plan restrictions.

Please see the [Fulfillment planning board](https://gitlab.com/groups/gitlab-org/-/boards/928182?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Fulfillment) for more details on what's next for Transactions.

<!-- ## 🏅 Competitive landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- ## Analyst landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ## Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## 🎢 Top user issues
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

* [Category issues listed by popularity](https://gitlab.com/gitlab-org/gitlab-ee/issues?label_name%5B%5D=transactions&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

## 🦊 Top internal customer issues/epics  
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

* [Findings from supporting upgrades/renewal queue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1519)

<!--  ## Top Vision Item(s)
What's the most important thing to move your vision forward?-->
