---
layout: markdown_page
title: "Category Vision - Service Desk"
---

- TOC
{:toc}

## Service Desk
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

Service desk allows your support and development teams to directly connect with your customers via email, with no external tools needed. Everything is done in GitLab. It pulls your customers directly into your DevOps process, since their feedback becomes issues right inside GitLab, potentially even in the same project where you are developing your product or service. 

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

Service desk currently works via email. Many similar support tools offer a web channel as well. In particular, if you submit feedback via a web form, you can more easily request and track structured fields and information. So the next feature is exactly that, allowing development teams' customers to contact them, [via a web form](https://gitlab.com/gitlab-org/gitlab-ee/issues/1734) served by their GitLab instance, with submissions that turn into issues, just like email feedback right.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Zendesk and Freshdesk are popular tools to do customer support management, and we can learn from these tools used by many organizations. Jira Service Desk leverages the existing Jira issue tracker to get customer tickets into Jira. 

GitLab Service Desk takes inspiration from Jira Service Desk to also get customer tickets into GitLab. In GitLab, the benefits are even more pronounced since a single customer ticket is turned into a GitLab issue which can be incorporated into both broader portfolio management features of GitLab, and also downstream with product-development team sprints. 

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts. 

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

Customers want to further use Service Desk like a CRM tool. One way to do this is to leverage GitLab labels to automatically filter tickets/issues as they arrive from customers. Another way is to start to track users as they send in issues. See [Automatically label Service Desk issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/2030) and [Customer Database for Service Desk](https://gitlab.com/gitlab-org/gitlab-ee/issues/2256). 

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Automatically labeling issues is popular with users. See [Automatically label Service Desk issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/2030). 

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

GitLabbers have found it difficult to use Service Desk since it is not powerful enough to do so currently. The legal team is using Service Desk and they are interested in [these issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Service%20Desk&author_username=jhurewitz). 

We have engaged with the GitLab Support Team (Lee M in particular) in the past to ask if they can adopt Service Desk. Unfortunately, we haven't had enough bandwidth to support incremental improvements to make this realistic. As we scale our product development teams and can prioritize Service Desk again, a pivotal goal will be re-engage the GitLab Support Team and establish a list of issues/features they need to use GitLab Service Desk and work together on a migration plan to help them eventually move off of Zendesk.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
- [Service desk with web form](https://gitlab.com/gitlab-org/gitlab-ee/issues/1734)