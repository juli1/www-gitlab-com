---
layout: markdown_page
title: "Product Vision - Plan"
---

- TOC
{:toc}

## Overview

The [GitLab DevOps lifecycle](/direction/#devops-stages) contains the Plan stage.
The product vision of Plan is to enable all people in any size organization to:

- **Innovate** ideas.
- Organize those ideas into transparent, **multi-level work breakdown plans** that cut
across departments.
- **Track the execution** of these plans, adjusting them along the way as needed.
- **Collaborate** with team members, internal and external stakeholders, and executive
sponsors, using the same set of single-source-of-truth artifacts in a single application.
- **Measure** the efficacy of the entire process, characterizing the flows with
**value streams**, thus providing actionable insight for continuous improvement.

This product vision is achieved through several **product categories** collected
into **groups**.

In the **Team Planning** group, we have the [**Project Management**](#project-management),
[**Kanban Boards**](#kanban-boards), and [**Time Tracking**](#time-tracking) product
categories. These are focused on helping individual product development teams deliver
tangible business value to their customers, by enabling them to ship software, and
in particular, the most important priorities, at a high velocity, sustainably over
a long-period of time (avoiding burnout). Kanban Boards will increasingly play a
crucial role here, with teams congregating within one or several boards that serve
as a central location for collaboration, progress tracking within a sprint, and
even retrospective evaluation afterward. Project Management improvements will continue
to be the glue that ties issues and their many attribute objects together, including
labels, milestones, weights, and assignees. Time Tracking is a strategic area to
incorporate management of the most imporant resource of Team Planning, namely that
of individuals and their valuable time.

In the **Enterprise Planning** group, we have [**Agile Portfolio Management**](#agile-portfolio-management),
and [**Value Stream Management**](#value-stream-management). Agile Portfolio Management
builds on Team Planning by helping _multiple_ teams deliver a coherent experience
for customers, with potentially many different product and services. In particular,
a _portfolio_ of initiatives are managed at the director level, and even at the
executive level. Agile Portfolio Management will thus be focused on work breakdown
plans that support both top-down and bottoms-up planning, as well as help business
sponsors make crucial decisions of where to invest resources that have longer-term
business ramifications, using ROI analysis, resource planning, financial management,
and other tools. Value Stream Management will help teams in an organization measure
their end-to-end cycle time of delivering value to customers, and even aggregate
that metric across different teams, and ultimately the entire organization. This
metric should be reduced as a best practice, and the vision of Value Stream Management
is to exactly enable that, by helping teams create and manage their custom DevOps
workflows, and identify where their most significant process inefficiencies lie,
in order to invest resources into mitigating those problems first.

In the **Certify** group, we have [**Requirements Management**](requirements-management),
[**Quality Management**](quality-management), and [**Service Desk**](service-desk).
Requirements Management will help organizations with more rigorous requirements
planning to track and manage changes and traceability of their intended software
(and even hardware) capabilities. Quality Management will help organizations maintain
processes and artifacts to manage and track software quality. Service Desk will
continue to help the customers of GitLab to easily send in feedback, whether
it be bugs, performance problems, or general feature requests. In particular, Service
Desk will be improved to truly realize the vision of bringing customers, support
teams, and product devlopment teams all closer together within one tool, in order
to reduce DevOps end-to-end cycle times and create the right software.

- [Timeline-based roadmap view of upcoming planned improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&layout=QUARTERS&scope=all&sort=start_date_asc&state=opened)
- [Upcoming planned strategic direction improvements](https://gitlab.com/groups/gitlab-org/-/boards/706864?label_name[]=direction)
and [upcoming planned improvements in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864)
for the next few milestones (monthly releases)

See many interesting features coming in 2019, as of late December 2018.

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/qC9H-BChiwQ" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
</figure>

<%= partial("direction/categories", :locals => { :stageKey => "plan" }) %>

## Goals and strategy

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/K2wlXStHXAo" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
</figure>

### Measurable goals

The overarching goal of Plan is to have **GitLab be the primary planning tool for
successful medium and large enterprises of the future**. In particular, see [specific timeline targets of category maturity levels](/handbook/product/categories/maturity/)
in Plan, representing goal metrics of 2019.

We also aim for the following placements in analyst reports (if they will be published
in the given year):

| Report | 2019 | 2020 | 2021 |
| --- | --- | --- | --- |
| [Gartner Enterprise Agile Planning Tools](https://www.gartner.com/doc/3872863/magic-quadrant-enterprise-agile-planning) | Visionaries | Leaders | Leaders, above all other providers |
| [Forrester Value Stream Management Tools](https://www.forrester.com/report/The+Forrester+New+Wave+Value+Stream+Management+Tools+Q3+2018/-/E-RES141538) | Strong Perfomers | Leaders | Leaders, above all other providers |

Our overaching goal is specific to successful companies of the future. This is a
goal aligned with the [vision of GitLab](https://about.gitlab.com/direction/#vision),
to **allow everyone to collaborate on all digital content**, since we believe successful
companies will drive the innovation of digital collaboration more broadly.

It's an ambitious and relevant goal since successful companies of the future will
be digital companies, with software becoming a key business enabler, if not a key
business driver. Since [software is eating the world](https://a16z.com/2011/08/20/why-software-is-eating-the-world/),
all successful businesses will undergo digital transformations. Conversely, a business
may fail for many reasons, and one of them will be a lack of digital transformation.

It's a goal with expansion built in, since we are currently in the midst of a digital
transformation. Silicon Valley style tech companies have already shown the software
industry how an Agile DevOps approach to product development is a significant benefit,
if not key differentiator, to growing a business. Now medium to large businesses
who use tech but aren't fundamentally tech companies across many different verticals
need to undergo digital transformations of their own. Those who fail to do so will
not survive. Those who are able to transform will have renewed opportunities. We
want GitLab to be at the heart of this transformation, helping all these businesses
realize the promise of Agile DevOps.

So while GitLab may serve businesses today, our focus is on becoming the primary
planning tool of successful companies of tomorrow. And that very distinction informs
our strategy.

### Future-focused strategy

Our strategy is _not_ to compete with existing vendors head-on today, and definitely
not on a feature-by-feature basis. Tools such as Jira, Trello, and Clarity CA have
been in development for a long time, and have amassed significant feature sets.
Convincing customers to choose GitLab over these tools based on feature comparisons
is not a good strategy, at least not in isolation. GitLab is a newer application
and does not have all these features. We would typically lose in these isolated
comparisons. Furthermore, by trying to achieve parity, we would be building features
focused on existing workflows of current companies who have been using these features
for a long time, even features that are antithetical to Agile DevOps.

Instead, our strategy is neatly summed up with three parts:

- Make big bets on the future.
- Mitigate risks by taking our existing customers on a journey.
- Leverage our single application strategy, in particular our existing SCM and CI
product areas.

We believe that the future of an Agile DevOps style transformation, as it pertains
to running a successful business, involves _continuously_ planning, breaking your
plans based on feedback, and re-planning. Planning is important to align your business
goals and internal operations. It's a way to bring some semblance of order in an
otherwise chaotic business context impacted by uncertainty from many sources. At
the same time, plans need to be continuously broken (voluntarily) to adapt to new
information. A successful company thus can perform this plan-break-plan-re-plan
cycle at a high velocity, capturing business opportunities ahead of competitors,
and even creating new markets along the way. This is exactly what we believe the
future of GitLab Plan facilitates. [Project Management](https://gitlab.com/groups/gitlab-org/-/epics/666)
and [Agile Portfolio Management](https://gitlab.com/groups/gitlab-org/-/epics/667)
(with [Kanban Boards](https://gitlab.com/groups/gitlab-org/-/epics/665) too) are
the key product categories that allow businesses to manage all this information.
We are architecting these information systems _not_ for rigidity, but for flexibility.
Plans are made to be broken. A business culture should incent constant revision
of plans based on the latest information available. And we believe GitLab should
be a tool that encourages that very ethos.

The plan-break-plan-re-plan cycle is only effective when it is done at a high velocity,
and that's exactly where [Value Stream Management (VSM)](https://gitlab.com/groups/gitlab-org/-/epics/668)
comes in. VSM is a methodology focused on minimizing end-to-end cycle time of software
delivery. The more you reduce your cycle time, the sooner you get customer and market
feedback, and the faster you can re-plan, adapting to customer needs and ultimately
the business climate. VSM thus gives you a clear target metric that is _directly_
related to your business agility and thus success. VSM also provides the framework
to reduce the cycle time itself. It helps teams identify inefficiencies in their
DevOps processes, thus allowing a methodical approach to process improvement. For
example, a team could always prioritize re-optimizing the stage that currently has
the longest stage in their DevOps process.

These are inherently big risky bets, because many customers are just starting their
digital transformations, and don't believe or recognize that they should be moving
to these future ways of working. We are building features that customers
may not want now, or may not even want in the future, because even though we have
high confidence of this future vision, we may not get all the granular details right.
At the same time, this prioritization also specifically means we are _not_ building
certain features that customers want now. So, to mitigate these risks, we are taking
customers on a journey, and traveling together. We are closely collaborating with
strategic customers who understand the future of Agile DevOps. In particular, these
are large enterprises who recognize the plan-break-plan-re-plan approach, and are
doing the best to adopt these new ways of working, even as they struggle internally
to make those transitions. We are partnering with these customers to shape our roadmap,
implement specific future-vision-type features, and having them closely participate
in our product development. Through these partnerships, we will identify
the details and build the relevant features that future successful businesses will
eventually adopt. And thus, we will win tomorrow, since we are building the future
today, ahead of competitors. In addition, some customers are open to new ways of
working, and trust GitLab for guidance. These are also key collaborators that have
an open mind and are willing to give GitLab's version of the future a shot, today.

The final part in our strategy is leverging our GitLab-wide single application approach.
If we are focused on strategic partnerships to invent the future, it means that
it is harder to win customers of today, especially if we are not catering to their
needs specifically. Fortunately, GitLab is already a leader in SCM and CI. Customers
are already buying GitLab for these features, and often these customers naturally
transition their project management and even portfolio management needs over to
GitLab over time. Furthermore, since GitLab pricing tiers do not silo product areas,
the friction of adoption for GitLab Plan is very low for these already-paying customers.
Thus, this already-installed-and-available advantage also offsets some risk of focusing
on the future, and increase usage of GitLab Plan today.

#### Problems with existing project management software

Over time, many of the market leaders in the project management space have implemented features
that have introduced a large amount of complexity into the system. Complexity introduces the following
challenges:

* Increases the barrier of entry to adopting the tool
* Limits the amount of people in an organization who can effectively use the tool
* Value out of using the tool is delayed for the organization
* Developers will shy away from the tool and/or loathe the fact they have to use it

A few features we see as problematic in the space are:

* Complex workflows, and the associated set up and management of them
* Complex permission structures
* Non-intuitive objects and resulting actions
* Customer query languages

We strive to have users getting value out of Plan within a few clicks. As such, we are strategically
not implementing features that may garner additional marketshare from legacy project management
tools in the short term. We believe taking a long term view and re-architecting this space is the
proper way to help software development and DevOps teams plan work.

## Jira integration

GitLab supports deep Jira integration, allowing teams to use Jira for issue
mangement, but still leveraging the benefits of GitLab source control and other
native GitLab features.

See [upcoming planned improvements for Jira integration](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=Jira).

## How we prioritize

We follow the [general prioritization process](/handbook/product/#prioritization)
of the Product Team. In particular, we consider reach, impact, confidence, and
effort to identify and plan changes in upcoming milestones (monthly iterations).

- See a high-level [timeline-based roadmap view of planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- See [upcoming planned strategic direction improvements](https://gitlab.com/groups/gitlab-org/-/boards/706864?label_name[]=direction)
and [upcoming planned improvements in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864)
for the next few milestones (monthly releases).

All GitLabbers use Plan features, more so than other GitLab stages, so it's important
we are continuing to support GitLabber workflows and prioritizing them.
[See issues scheduled for the next few milestones that directly benefit GitLabbers.](https://gitlab.com/groups/gitlab-org/-/boards/1003901)

## How we execute week to week

There is a video call weekly meeting where any stakeholder can bring up topics relevant
to Plan. Usually, at least the product manager, engineering managers, and designers
attend this meeting. Engineers will often attend this meeting (if they can, depending
on timezones), as well as other folks such as product marketing. Throughout the
week, if there are other video calls necessary to discuss specific topics, [Plan folks](/handbook/product/categories/#plan-stage)
create them, inviting specific individuals to collaborate, but they are open for
all to attend.

- See [agenda points](https://docs.google.com/document/d/1cbsjyq9XAt9UYLIxDq5BYFk47VA5aaTeHfkY2dttqfk/edit) for these calls.
- See [YouTube videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KoceqcTneOVmAzhEp6NinY0) of these calls.

All other collaboration happens asynchronously in GitLab issues and Slack (#g_plan).

## Contributions and feedback

We love community code contributions to GitLab. Read
[this guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/index.md)
to get started.

Please also participate in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce).
You can file bugs, propose new features, suggest design improvements, or
continue a conversation on any one of these. Simply [open a new issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new)
or comment on [an existing one](https://gitlab.com/gitlab-org/gitlab-ce/issues).

## Upcoming Releases

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "plan" }) %>
