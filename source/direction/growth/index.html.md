---
layout: markdown_page
title: Product Vision - Growth
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Growth. If you'd like to discuss this vision
directly with the product leader for Growth, feel free to reach out to Eric
Brinkman via [e-mail](mailto:ebrinkman@gitlab.com) or [Twitter](https://twitter.com/ericbrinkman).
We are also hiring for a [Product Director](https://boards.greenhouse.io/gitlab/jobs/4202773002) and several [Senior Product Managers](https://boards.greenhouse.io/gitlab/jobs/4206239002)
for our Growth groups. Please visit our [jobs page](https://jobs.gitlab.com) to find out
more and apply.

## Overview & Philosophy

Growth is responsible for scaling GitLab usage by connecting users to the
existing value that GitLab already delivers. This is in contrast to the other
groups in our organization that are primarily concerned with creating value.
Growth analyzes the entire customer journey from acquisition of a customer, to
the flow across multiple GitLab features, and even reactivation of lost users.
Growth is commonly asking and finding out the answers to these types of
questions:

* Why are so many users signing up for GitLab but never logging in?
* How do we successfully attract new customers to GitLab?
* How do we successfully win back customers that once used and/or paid for GitLab but have canceled their subscription?
* What is the first action taken after logging in and is it the right one for maximum connection to value?
* How can we grow the amount of customers that use all stages of GitLab?

## Growth Strategy  

Growth is expected to always start with a hypothesis. The hypothesis should be
fully defined prior to execution in order to safeguard from "bad science," namely
data that may correlate but not have any causal relationship. After a hypothesis has been
defined, Growth relies on two primary constructs to make decisions: analysis of data, or
what people do in the application, and qualitative research, in essence the
“why” behind the data.

Growth should focus on metrics that are important at the company level, while also considering
company strategy. We have broken down the primary metrics of interest in the various Growth functions below.
Other metrics of interest may surface during Growth exploration, and they may become important to the company,
but this is a byproduct of the process, rather than a goal.

### Growth Structure

Growth will operate in small groups aligned around the four growth functions
listed below. Each group will consist of a product manager, engineering manager,
some full-stack engineers, a (shared) data analyst, and a (shared) UX designer.
Each group member will have a functional reporting structure, in order to report
to a leader that understands their job function.

#### Working across stages

The hypotheses explored by Growth will span across groups and stages. How do other groups interact with Growth?

Growth does not own areas of the product, but finds ways to help users discover and unlock value throughout the GitLab 
application. They will do a combination of shipping low-hanging fruit themselves, but will partner with other stages 
on larger initiatives that need specialized knowledge or ongoing focus.

It is the responsibility of other stages to maintain and expand core value in their respective product areas; it's 
Growth's mission to streamline delivery of that value to users. For example:

* [Manage](https://about.gitlab.com/handbook/product/categories/#manage-stage) is responsible for user registration and 
management. This stage maintains a long-term vision on how users are created and maintained in GitLab, with a roadmap that
includes other categories and priorities.
* Growth may identify user registration as an opportunity to further the group's [priorities](#growth-priorities). Growth's
engineering team may ship smaller improvements independently of Manage (such as [soft email confirmation](https://gitlab.com/gitlab-org/growth/issues/72)), but may need to partner with Manage on larger efforts like a completely [new onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/57832) experience.

### Why Invest in a Growth Team in 2019?

We believe we have found product-market fit by providing a single application
for the entire DevOps lifecycle, highlighting the value created when teams don’t
have to spend time integrating disparate tools. Now that product-market fit has
been achieved, we must do a better job of connecting our value to our customers.
Therefore, a Growth focus is required. Additionally, we are beginning to put
more strategic focus on the growth of GitLab.com, which is a different user
signup and activation process than self-managed instances, which typically
involves direct sales.

## Growth Groups

### Composition

Each group will consist of:

1. Product Manager
1. 2 engineers
1. Data analyst
1. UX person
1. Product marketing manager (maybe shared between groups)

### Activation

Primary metric: Acquired and activated users, last 30 days

### Adoption

Primary metric: [Stage Monthly Active Users (SMAU)](/handbook/product/growth/#smau)

### Upsell

Primary metric: Percent (%) of users who went to a (higher) subscription

### Retention

Primary metric: Retention, including reactivation

## Growth Priorities

One of the first priorities of Growth is making sure we have enough data to do
regression monitoring. At any point in time, we should be able to answer: Are we
making anything worse?

We'll start by generating dashboards for the primary metrics above and other [KPIs for Product](/handbook/ceo/kpis/#vp-of-product).

By analysing the key metrics, which are really part of a full [funnel analysis](#funnel),
we can identity hotspots to tackle first. Then proceed in a scientific fashion
with hypotheses and experimentation.

## Funnel

1. Visit
1. Aquisition (Signup)
1. Activation (Key usage)
1. Adoption (Increasd stage usage)
1. Converstion
1. Upsell
1. Churn (!Retention)
1. Reactivation

## Growth Inspiration

While the Growth function is becoming more common in late stage, well funded
software companies, it is still a relatively nascent discipline. At GitLab, we
seek to derive best practices from the industry and adapting them to work with
our culture. Here are a few articles we’ve found helpful as we are formulating a
Growth strategy and vision:

* [What Are Growth Teams For, and What Do They Work On?](https://news.greylock.com/what-are-growth-teams-for-and-what-do-they-work-on-a339d0c0dee3)
* [Sustainable Product Growth](https://www.sequoiacap.com/article/sustainable-product-growth)
