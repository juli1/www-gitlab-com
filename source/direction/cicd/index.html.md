---
layout: markdown_page
title: "Product Vision - CI/CD"
---

- TOC
{:toc}

### Overview

The CI/CD sub-department focuses on code [build/verification](/direction/verify), [packaging](/direction/package), 
and [delivery](/direction/release). Each of these areas has their own strategy page with upcoming features, north
star directions, and more. This page ties them together via important concepts that unify the direction across
all of these areas.

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_extended_11_11.png "Pipeline Infographic")

### Themes

There are two main themes that CI/CD at GitLab is oriented towards: Progressive Delivery, and a kind of "Grand
Unified Theory of CI/CD" oriented around some powerful, integrated primitives.

#### Progressive Delivery

Put simply, Progressive Delivery is a core set of ideas and emerging best practices, oriented around
being able to control and monitor deployments in stages over time and in an automated and safe
way. GitLab is at a unique advantage when it comes to enabling this because, until now, Continuous
Delivery solutions have often required integrating various point products to provide each of the
individual capabilities needed. Here at GitLab, we're going to be implementing and
integrating these features within our single application, with Progressive Delivery in mind from the start.

Additional articles on Progressive Delivery can be found on the [LaunchDarkly blog](https://launchdarkly.com/blog/progressive-delivery-a-history-condensed/),
[RedMonk](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/) and 
[The New Stack](https://thenewstack.io/the-rise-of-progressive-delivery-for-systems-resilience/).

[Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/) and [Continuous Delivery](https://about.gitlab.com/direction/release/continuous_delivery/) are
our core categories for Progressive Delivery, but there are additional ones that will be important
in providing our comprehensive solution:

- [Feature Flags](https://about.gitlab.com/direction/release/feature_flags/) for targeted rollouts
- [Review Apps](https://about.gitlab.com/direction/release/review_apps/) for on-demand validation environments
- [Tracing](https://gitlab.com/groups/gitlab-org/-/epics/89) for behavior analysis across progressive deployments

We truly believe Progressive Delivery is the future of software delivery, and we plan to deliver an MVC
solution soon: [gitlab-org#1198](https://gitlab.com/groups/gitlab-org/-/epics/1198)

#### Powerful, Integrated Primitives

Our vision for CI/CD remains aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book.
The ideas of reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers
to getting things done has stood the test of time. This builds on the individual [Verify](/direction/verify), [Package](/direction/package), 
and [Release](/direction/release) stage direction pages.

We think there are a few simple but powerful primitives to add to GitLab CI/CD which will allow us to achieve this vision.
Each of these works independently and solves problems from simple to advanced, but truly become powerful when used in concert.
Because everything in CI/CD begins with the pipeline, that's where we focus our attention.

- **Workspaces**: Persisting artifacts between jobs in a pipeline (and even publishing them for later download) is done using a mix of caching
  and artifact functionality in GitLab. Neither is really ideal for the purpose - cache is not guaranteed to be available, and
  artifacts are more heavyweight than they need to be for this usage. Instead, we are going to implement workspaces as a first-class
  concept in GitLab which will support parallelization across jobs and make it easier to have your pipelines run just like they
  would if you had run it locally.

- **Directed Acyclic Graph (DAG)**: There's a simple graph model in GitLab today, but this works by assuming everything in a single stage
  runs in parallel, and every job in the following stage must wait for all the jobs in the previous stage to complete. This is
  functional, but not sufficient for more complex workflows and ones where every minute counts when waiting. By converting
  pipelines into DAGs, we unlock as-fast-as-possible execution as well as multiple success paths within a single pipeline (for
  monorepos, for example.)

- **Pipeline Entity Model**: Pipelines today can internally track a global success/failure status and generate artifacts,
  but have no way to really reflect on those and provide more advanced behaviors based on the internal state of the pipeline. By
  adding typed artifacts (a container image, a .json file, a gemfile) and internal state (the yaml used to run the job, status) as
  something available for introspection at runtime or to be used as a dependency in the DAG, we open the door to incredibly advanced
  cross or per-project pipeline behaviors based on generation of actual dependencies. As an additional benefit, this collection of
  entities within the pipeline itself becomes the bill of materials, collecting auditing and compliance evidence for the release as it runs.

- **Namespaces for Includes**: Finally, we have includes and extends capabilities within the configuration YAML today, but these
  work by importing the included/extended content into a global YAML, creating the risk of namespace collisions or otherwise
  unintended impacts. By adding an optional way to namespace includes, it allows you to build includes in a more rational way, much like
  you would create functions in code using locally scoped variables. This also makes includes more portable, making them easier
  to share and redistribute. We also plan to enable auto-inclusion of configuration YAML from subfolders, making this even simpler
  for monorepo and other scenarios.

In addition to the obvious direct benefits, these reinforce each other. By improving includes we make the DAG syntactically and 
cognitively easier to use. A pipeline entity model allows the DAG to self-reflect and define its own behavior based on the progress
it makes. By simplifying workspaces and making them parallelizable we make them available to an efficient DAG that is running as
much in parallel as possible.

We plan to deliver an MVC for these integrated primitives soon: [gitlab-org#1199](https://gitlab.com/groups/gitlab-org/-/epics/1199)

If you're interested in learning more, check out this deep dive video where we discuss in detail how these ideas related to each other.

<figure class="video_container">
<iframe src="https://youtube.com/embed/FURkvXiiJek" frameborder="0" allowfullscreen="true" width="320" height="180"> </iframe>
</figure>
