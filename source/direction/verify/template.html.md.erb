---
layout: markdown_page
title: "Product Vision - Verify"
---

- TOC
{:toc}

The Verify stage of the DevOps pipeline
covers the CI lifecycle as well as testing (unit, integration, acceptance,
performance, etc.). Our mission is to help developers feel confident in
delivering their code to production. Please read through to understand where
we're headed and feel free to jump right into the discussion by commenting on
one of the epics or issues.  If you'd like to discuss this vision directly with the product manager for Verify, 
feel free to reach out to Brendan O'Leary via [e-mail](mailto:boleary@gitlab.com), [Twitter](https://twitter.com/olearycrew), or by [scheduling a video call](https://calendly.com/brendan-gitlab/chat).

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

Note that this stage is also involved in delivering improvements for our
[mobile development and delivery](/direction/mobile) and [CI/CD](/direction/cicd) 
use cases. If you have an interest in these topics, please also take a look there.

## Overview

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/_AgVU5Yi850?rel=0" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
</figure>

## North Stars

### Gateway to Operations

CI was one of the first "additions" to GitLab where there was a hotly contested debate about placing it 
into a single application.  As the first stage in the [Ops Department](/handbook/product/categories/#ops-department), 
Verify has the unique ability to tie together what developers are doing (every commit) with what 
operators need (reliable, tested deployments). Therefore, Verify will continue to strive to be a "shining city on a hill" of the 
tangible and emergent [benefits of a single application](/handbook/product/single-application/), 
avoiding the pitfalls of legacy systems that optimize only for one stage of the DevOps lifecycle.

Related Epics:
* [Gateway to Operations parent epic](https://gitlab.com/groups/gitlab-org/-/epics/814)
  * [Lock down path to production](https://gitlab.com/groups/gitlab-org/-/epics/762)
  * [Variables and secrets management](https://gitlab.com/groups/gitlab-org/-/epics/816)

### Speed & Scalability

At the core, GitLab CI/CD was built from the ground up with speed and scalability in mind. This is reflected both in the product design and product vision 
([automated testing, testing all the things](https://about.gitlab.com/2018/02/05/test-all-the-things-gitlab-ci-docker-examples/))
To enable our users to go from monthly shipping releases to truly enabling continuous delivery, everything that we do must be concerned with the scalability and speed of continuous integration and testing.

Related Epics
* [Speed & Scalability parent epic](https://gitlab.com/groups/gitlab-org/-/epics/786)
  * [Define and record Verify time to first byte](https://gitlab.com/groups/gitlab-org/-/epics/830)
  * [More efficient pipeline execution](https://gitlab.com/groups/gitlab-org/-/epics/439)

### Lovable solutions to complex problems

Our users are solving some of the hardest problems on the cutting edge of software development. The promise of GitLab overall is to provide a simple, lovable application for the entire DevOps lifecycle. Many parts of that cycle - including Verify - are traditionally complex to solve.  Our goal is to provide a lovable solution to these complex problems so that users focus on their code - not building a DevOps environment.

Related Epics:
* [Make CI Lovable parent epic](https://gitlab.com/groups/gitlab-org/-/epics/811)
  * [Lovable for microservices](https://gitlab.com/groups/gitlab-org/-/epics/813)
  * [Lovable for monorepos](https://gitlab.com/groups/gitlab-org/-/epics/812)
  * [Review and Overhaul CI/CD Docs](https://gitlab.com/groups/gitlab-org/-/epics/784)
  * [UX improvements](https://gitlab.com/groups/gitlab-org/-/epics/295)
    

## Our User's Journey to DevOps Maturity

Often, when businesses invest in continuous integration, they can plateau 
once they have have the code building and tests running. However, to deploy effectively, 
we need to embrace code review, dependency assurance, security scanning and performance metrics for every commit. 
GitLab's vision for Verify is to help users accelerate their journey to DevOps maturity. 

Related Epics:
* [Review and Overhaul CI/CD Docs](https://gitlab.com/groups/gitlab-org/-/epics/784)
* [Verify UX research](https://gitlab.com/groups/gitlab-org/-/epics/592)
* [CI/CD Reporting & Analytics](https://gitlab.com/groups/gitlab-org/-/epics/358)

<%= partial("direction/categories", :locals => { :stageKey => "verify" }) %>

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Verify at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=devops%3Averify);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the sections below.

## Upcoming Releases

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "verify" }) %>
