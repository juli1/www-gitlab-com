---
layout: markdown_page
title: "Category Vision - Accessibility Testing"
---

- TOC
{:toc}

## Accessibility Testing

Beyond being a compliance requirement in many cases, accessibility testing is the right thing to do for your users. Accessibility testing is similar to UAT or Usability (which we track at [gitlab-org#490](https://gitlab.com/groups/gitlab-org/-/epics/490)), but we see accessibility as a primary concern of its own.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=accessibility%20testing&sort=milestone)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

Up next is [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) (accessibility testing for review apps MVC) which establishes our footing in the accessibility testing space. This will enable us to start offering out-of-the-box accessibility testing via artifacts, and provide a foundation for us to build more features on top of such as making the accessibility results embedded in the MR widget or provide more detailed CI views for accessibility testing.

## Competitive Landscape

Competitors in this space are not providing first-party accessibility testing platforms, but do integrate with pa11y or other tools to generate results via the CI/CD pipeline. Implementing [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) (accessibility testing for review apps MVC) will help us meet (or in many cases exceed) competitor capability be embedding it into our review apps, a natural home for them.

## Analyst Landscape

We do not engage with analysts for acceptance testing market analysis.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category yet.

## Top Customer Issue(s)

There are no top customer issues for this category yet.

## Top Internal Customer Issue(s)

Internal customers are working on a VPAT (voluntary assessment) at [gitlab-ce#39662](https://gitlab.com/gitlab-org/gitlab-ce/issues/39662) which will help us answer regulatory questions in certain cases. Additional research is needed to determine if [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) will help us in those cases; I believe it will but that must be confirmed.

A related Epic is the GitLab accessibility epic that contains items about making GitLab itself more accessible: [gitlab-org#567](https://gitlab.com/groups/gitlab-org/-/epics/567).  Loveable for this category is defined as being able to use GitLab to detect all of those issues in an automated way and see that they are addressed when fixed.

## Top Vision Item(s)

Similar to other sections where this issue is repeated, [gitlab-ce#54806](https://gitlab.com/gitlab-org/gitlab-ce/issues/54806) will establish our presence with out-of-the-box accessibility testing.