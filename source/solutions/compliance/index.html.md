---
layout: markdown_page
title: "Building applications that meet common regulatory compliance standards"
---
## Compliance without friction

GitLab helps teams achieve and demonstrate compliance with their specific IT controls.

1. Auditing, logging, traceability and reporting
1. Visible code reviews
1. Merge approvals (change approvals)
1. Access control and granular permissions
1. Controlled / protected pipelines
1. Security scanning and License Management for every commit
   1. SAST
   1. DAST
   1. Container
   1. Dependency
1. Security dashboard - track and manage vulnerabilities across projects in one place

See how GitLab helps with specific compliance and framework requirements:
   * [PCI Compliance](../pci-compliance)
   * [GDPR](../../gdpr)  
   * [IEC 62304:2006](../iec-62304)
   * [ISO 13485:2016](../iso-13485)
