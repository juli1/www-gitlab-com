**Review Apps**: https://release-X-Y.about.gitlab.com/YYYY/MM/22/gitlab-X-Y-released/

_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/YYYY_MM_22_gitlab_X_Y_released.yml
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Features YAML Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/images/feature_page/screenshots
- **Homepage card**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/includes/home/ten-oh-announcement.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/community/release-managers/

| Release post manager | Tech writer | Copy edit | Messaging | Social |
| --- | --- | --- | --- | --- |
| `@mention` | `@mention` | `@mention` | `@mention` | `@mention` |

### General contributions

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

All contributions added by team members, collaborators, and Product Managers (PMs).

#### Author's checklist

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

The PM leading the post is responsible for adding and checking the following items: `@mention`

- [ ] Label MR: ~"blog post" ~release ~"release post" ~P1
- [ ] Assign the MR to yourself
- [ ] Add a release post retrospective issue ([example](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3936)) and provide a link to it at the top of this MR description with the text `Process Improvements? Have suggstions for improving the release post process as we go? Capture them in the [Retrospective issue]().`
- [ ] Check the reviewers on Slack and add their names (and your own) replacing each `@mention` in the MR description checklists
- [ ] Add milestone
- [ ] Update the links and due dates in this MR description
- [ ] Make sure the blog post have all initial files, as well as this MR template contains the latest template
- [ ] Add authorship (author's data)
- [ ] Ask the messaging lead to add the introduction by the 6th working day before the 22nd
- [ ] Add [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) (MVP block)
- [ ] Add MVP to `data/mvps.yml`
- [ ] Make sure the PM for the MVP feature adds a corresponding feature block if applicable, linking from the MVP section
- [ ] Add [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image) (`image_title`) (compressed)
- [ ] Add [social sharing image](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image) (`twitter_image`) (compressed)
- [ ] Make sure all feature descriptions are positive and cheerful
- [ ] Make sure all features listed in the [direction](https://about.gitlab.com/direction/)
page are included in the post
- [ ] Mention the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) asking them
if there's any relevant info to be added to the [upgrade warning](https://about.gitlab.com/handbook/marketing/blog/release-posts#upgrade-warning) section. If not, delete the `upgrade` block in the relese post data file
- [ ] Mention managers to remind them to add their team performance improvements: @gitlab-com/backend-managers and @gitlab-org/frontend/frontend-managers
- [ ] Check with the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) on what the top features should be.
- [ ] Check if deprecations are included
- [ ] Alert people one working day before each due date (post a comment to #release-post Slack channel)
- [ ] Make sure all images (png, jpg, and gifs) are smaller than 300 KB each
- [ ] Run the release post through an automated spell and grammar check
- [ ] Pull `master` into the release post branch and resolve any conflicts
- [ ] Perform the [content review](#content-review)

#### Recurring blocks

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

The following sections are always present, and managed by the PM or Eng lead
owning the related area:

- [ ] Mention the below individuals after feature freeze
- [ ] Add GitLab Runner improvements: `@brendan`
- [ ] Add Omnibus improvements: `@ebrinkman`
- [ ] Add Mattermost update to the Omnibus improvements section: `@ebrinkman`
- [ ] Add Performance improvements: `@DouweM`, `@smcgivern`, `@ayufan`, `@yorickpeterse`

#### Feature blocks

**Due date: YYYY-MM-DD** (6th working day before the 22nd)

The Product Managers are responsible for adding their feature blocks to the
release post by the due date for general contributions. PMs are also responsible
for adding any notable Community Contributions.

PMs: please check your box only when **all** your features and deprecations were
added with completion (documentation links, images, etc). Pls don't check if
there are still things missing.

Note: be sure to reference your Direction items and Release features. All items which appear
in our [Upcoming Releases page](/upcoming-releases/) should be included in the relevant release post.

- [ ] Mention the below individuals after feature freeze
- [ ] Manage (`@jeremy`)
- [ ] Plan (`@ebrinkman`)
- [ ] Create (`@jramsay`)
- [ ] Verify (`@brendan`)
- [ ] Package (`@trizzi`)
- [ ] Release (`@jlenny`)
- [ ] Secure (`@kencjohnston`)
- [ ] Configure (`@danielgruesso`)
- [ ] Monitor - APM (`@kencjohnston`)
- [ ] Monitor - Debug & Health (`@sarahwaldner`)
- [ ] Fulfillment (`@tipyn`)
- [ ] Distribution (`@ebrinkman`)

Tip: make your own checklist:

- Primary features
- Improvements (secondary features)
- Deprecations
- Documentation updated
- Documentation links added to the post
- Community contributions documented and added to the post
- Illustrations added to the post (compressed, max width = 1000 pixels)
- Update `features.yml` (with accompanying screenshots)

### Review

Ideally, complete the review until the **4th** working day before the 22nd,
so that the 3rd and the 2nd working day before the release
could be left for fixes and small improvements.

#### Content review

**Due date: YYYY-MM-DD** (5th working day before the 22nd)

Performed by the author of the post: `@mention`

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] Check all comments in the thread (make sure no contribution was left behind)
- [ ] Check Features' names
- [ ] Check Features' availability (Core, Starter, Premium, Ultimate badges)
- [ ] Reorder primary and secondary features according to their relevance to the user (most impactful features come first)
- [ ] Check Documentation links (all feature blocks contain `documentation_link`)
- [ ] Make sure `documentation_link` links to feature webpages when available
- [ ] Features were added to `data/features.yml` (with accompanying screenshots)
- [ ] Check all images size < 300KB (compress them all with [TinyPNG](https://tinypng.com/) or similar tool)
- [ ] Pull `master`, resolve any conflicts
- [ ] Make sure all discussions in the thread are resolved
- [ ] Mention `@markpundsack`, `@kencjohnston`, `@jlenny`, and `@ebrinkman` to remind them to review
- [ ] Assign the MR to the next reviewer (structural check)
- [ ] Lock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) with File Locking **on the 21st**

#### Structural check

**Due date: YYYY-MM-DD** (3rd working day before the 22nd)

The [structural review](https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check) is performed by a technical writer: `@mention`

- [ ] Add the label ~review-structure
- [ ] Pull `master` into the release post branch and resolve any conflicts
- [ ] Check [frontmatter](https://about.gitlab.com/handbook/marketing/blog/release-posts/#frontmatter) (entries, syntax)
- [ ] Check `image_title` and `twitter_image`
- [ ] Check image shadow applied correctly in release post data. Look for `image_noshadow: true` when an image already has shadow
- [ ] Ensure videos and iframes are wrapped in `<figure class="video_container">` tags (for responsiveness)
- [ ] Ensure all text is consistently hard wrapped at a suitable column boundary
- [ ] Check headings are in sentence case
- [ ] Check feature and product names are in capital case
- [ ] Check if images are harmonic/consistent
- [ ] Check any `.gitkeep` files and delete them if any
- [ ] Add or check cover img reference (at the end of the post). Data in `cover_img` in release post Yaml file
- [ ] Ensure content is balanced between the columns (both columns are even)
- [ ] Ensure links have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text)
- [ ] Check for badges consistency (applied to all headings)
- [ ] Double check that all features are documented
- [ ] Check that all links to about.gitlab.com content are relative URLs
- [ ] Check the anchor links in the intro. All links in the release post markdown file should point to something in the release post Yaml file
- [ ] Remove any remaining instructions (comments) from the release post files
- [ ] Run a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf) and ping reporters directly on Slack asking them to fix broken links
- [ ] Update the release post templates and release post handbook with anything that comes up during the process
- [ ] Pull `master` into the release post branch and resolve any conflicts (again)
- [ ] Report any problems from structural check in the `#release-post` channel by pinging the reporters directly for each problem. Do NOT ping `@all` or `@channel` nor leave a general message that no one will pay attention to. If possible, ensure any issues are tracked by open discussions in the merge request
- [ ] Post a comment in the `#general` channel linking to the review app + merge request reminding the team to take a look at the release post and to report problems in `#release-post`
- [ ] Remove the label ~review-structure.
- [ ] Assign the MR to the next reviewer (copyedit).

#### Further reviews

**Due date: YYYY-MM-DD** (2nd working day before the 22nd)

- [ ] Copyedit (content team): `@mention`
  - [ ] Title
  - [ ] Description
  - [ ] Intro
  - [ ] Grammar, spelling, clearness (body)
  - [ ] Update [homepage card](https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/includes/home/ten-oh-announcement.html.haml)
  - [ ] Tweet social sharing text (for Twitter, FB, and LinkedIn)
  - [ ] Assign the MR to the next reviewer (marketing)
- [ ] Marketing review (PMMs - [messaging lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead)): `@mention`
  - [ ] Write the introduction (**by the 6th working day before the 22nd**)
  - [ ] Check/copyedit feature blocks
  - [ ] Check/copyedit `features.yml`
  - [ ] Check/copyedit homepage blurb
  - [ ] Check/copyedit social sharing text
  - [ ] Mention Mark P (`@markpundsack`) for final check
  - [ ] Remove the label ~review-in-progress
  - [ ] Assign the MR back to the author

### On the 22nd

The author of the post is responsible for merging the MR and following up
with possible adjustments/fixes: `@mention`.

#### At 12:00 UTC

- [ ] Read the [important notes](#important-notes) below
- [ ] At ~12:30 UTC, ping the release managers on the `#releases` Slack
channel asking if everything is on schedule, and to coordinate timing with them:
  - If anything is wrong with the release, or if it's delayed, you must ping
  the messaging lead on `#release-post` so that they coordinate anything scheduled
  on their side (e.g., press releases, other posts, etc).
  - If everything is okay, the packages should be published at [13:30 UTC](https://gitlab.com/gitlab-org/release-tools/blob/master/templates/monthly.md.erb#L155), and available
  publicly around 14:10 UTC.
  - Ask the release managers to ping you when the packs are publicly available (and GitLab.com is up and running on the release version)
- [ ] Mention `@sumenkovic` to remind him to send the swag pack to the MVP
- [ ] Check if all the anchor links in the intro are working
- [ ] Check if there are no broken links in the review app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Pull `master` and fix any conflicts
- [ ] Check if there isn't any alert on Slack's `#release-post` and `#general` channels
- [ ] Check if there isn't any alert on this MR thread
- [ ] Check if the tweet copy is ready and someone is ready to share on social media

#### At 13:50 UTC

- [ ] Check if there aren't any conflicts. If there are any, pull `master` again and fix them.

Once the release manager confirmed that the packages are publicly available:

- [ ] Unlock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) just before merging.
- [ ] Merge the MR at 14:10-14:20 UTC.
- [ ] Wait for the pipeline. It should take ~40min to complete.
- [ ] Check the look on social media with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/).
- [ ] Check for broken links again once the post is live.
- [ ] Share on social media (or make sure someone else does) only when you're sure everything is okay.
- [ ] Share the links to the post and to the tweet on the `#release-posts` and
`#general` on Slack.

#### Important notes

- The post is to be live on the **22nd** at **15:00 UTC**. It should be merged and
as soon as GitLab.com is up and running on the new release version (or the latest
RC that has the same features as the release), and once all packages are publicly
available. Not before. Ideally, merge it around 14:20 UTC as the pipeline takes
about 40 min to run.
- The usual release time is **15:00 UTC** but it varies according to
the deployment. If something comes up and delays the release, the release post
will be delayed with the release.
- Coordinate the timing with the
[release managers](https://about.gitlab.com/community/release-managers/). Ask them to
keep you in the loop. Ideally, the packages should be published around
13:30-13:40, so they will be publicly available around 14:10 and you'll
be able to merge the post at 14:20ish.
- Once the post is live, wait a few minutes to see if no one spot an error
(usually posted in #general), then share on Twitter, Facebook, and LinkedIn,
or make sure someone (Emily vH, JJ, Marcia) does. Coordinate the social sharing
with them **beforehand**.
- Keep an eye on Slack and in the blog post comments for a few hours to make sure
no one found anything that should be fixed.
